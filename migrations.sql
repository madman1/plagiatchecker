CREATE SCHEMA `antiplagiat` ;

CREATE TABLE `antiplagiat`.`abbreviations` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Value` VARCHAR(100) NULL,
  `DotFirst` BIT NULL,
  PRIMARY KEY (`Id`),
  UNIQUE INDEX `Value_UNIQUE` (`Value` ASC));

insert into abbreviations(Value, DotFirst) values('ак.', false);
insert into abbreviations(Value, DotFirst) values('акк.', false);
insert into abbreviations(Value, DotFirst) values('абс.', false);
insert into abbreviations(Value, DotFirst) values('обл.', false);
insert into abbreviations(Value, DotFirst) values('пр.', false);
insert into abbreviations(Value, DotFirst) values('просп.', false);
insert into abbreviations(Value, DotFirst) values('гг.', false);
insert into abbreviations(Value, DotFirst) values('тыс.', false);
insert into abbreviations(Value, DotFirst) values('тис.', false);
insert into abbreviations(Value, DotFirst) values('чел.', false);
insert into abbreviations(Value, DotFirst) values('экз.', false);
insert into abbreviations(Value, DotFirst) values('обр.', false);
insert into abbreviations(Value, DotFirst) values('ум.', false);
insert into abbreviations(Value, DotFirst) values('род.', false);
insert into abbreviations(Value, DotFirst) values('др.', false);
insert into abbreviations(Value, DotFirst) values('см.', false);
insert into abbreviations(Value, DotFirst) values('ср.', false);
insert into abbreviations(Value, DotFirst) values('грн.', false);
insert into abbreviations(Value, DotFirst) values('долл.', false);
insert into abbreviations(Value, DotFirst) values('руб.', false);
insert into abbreviations(Value, DotFirst) values('мед.', false);
insert into abbreviations(Value, DotFirst) values('мин.', false);
insert into abbreviations(Value, DotFirst) values('сек.', false);
insert into abbreviations(Value, DotFirst) values('напр.', false);
insert into abbreviations(Value, DotFirst) values('нед.', false);
insert into abbreviations(Value, DotFirst) values('ст.', false);
insert into abbreviations(Value, DotFirst) values('хим.', false);
insert into abbreviations(Value, DotFirst) values('хім.', false);
insert into abbreviations(Value, DotFirst) values('устар.', false);
insert into abbreviations(Value, DotFirst) values('отеч.', false);
insert into abbreviations(Value, DotFirst) values('мол.', false);
insert into abbreviations(Value, DotFirst) values('зоол.', false);
insert into abbreviations(Value, DotFirst) values('гл.', false);
insert into abbreviations(Value, DotFirst) values('англ.', false);
insert into abbreviations(Value, DotFirst) values('нем.', false);
insert into abbreviations(Value, DotFirst) values('амер.', false);
insert into abbreviations(Value, DotFirst) values('А.', false);
insert into abbreviations(Value, DotFirst) values('Б.', false);
insert into abbreviations(Value, DotFirst) values('В.', false);
insert into abbreviations(Value, DotFirst) values('Г.', false);
insert into abbreviations(Value, DotFirst) values('Д.', false);
insert into abbreviations(Value, DotFirst) values('Е.', false);
insert into abbreviations(Value, DotFirst) values('Ж.', false);
insert into abbreviations(Value, DotFirst) values('З.', false);
insert into abbreviations(Value, DotFirst) values('И.', false);
insert into abbreviations(Value, DotFirst) values('Й.', false);
insert into abbreviations(Value, DotFirst) values('К.', false);
insert into abbreviations(Value, DotFirst) values('Л.', false);
insert into abbreviations(Value, DotFirst) values('М.', false);
insert into abbreviations(Value, DotFirst) values('Н.', false);
insert into abbreviations(Value, DotFirst) values('О.', false);
insert into abbreviations(Value, DotFirst) values('П.', false);
insert into abbreviations(Value, DotFirst) values('Р.', false);
insert into abbreviations(Value, DotFirst) values('С.', false);
insert into abbreviations(Value, DotFirst) values('Т.', false);
insert into abbreviations(Value, DotFirst) values('У.', false);
insert into abbreviations(Value, DotFirst) values('Ф.', false);
insert into abbreviations(Value, DotFirst) values('Х.', false);
insert into abbreviations(Value, DotFirst) values('Ц.', false);
insert into abbreviations(Value, DotFirst) values('Ч.', false);
insert into abbreviations(Value, DotFirst) values('Ш.', false);
insert into abbreviations(Value, DotFirst) values('Щ.', false);
insert into abbreviations(Value, DotFirst) values('Э.', false);
insert into abbreviations(Value, DotFirst) values('Ю.', false);
insert into abbreviations(Value, DotFirst) values('Я.', false);
insert into abbreviations(Value, DotFirst) values('etc.', false);
insert into abbreviations(Value, DotFirst) values('dr.', false);
insert into abbreviations(Value, DotFirst) values('mr.', false);
insert into abbreviations(Value, DotFirst) values('mrs.', false);
insert into abbreviations(Value, DotFirst) values('ms.', false);
insert into abbreviations(Value, DotFirst) values('inc.', false);
insert into abbreviations(Value, DotFirst) values('vol.', false);
insert into abbreviations(Value, DotFirst) values('et.', false);
insert into abbreviations(Value, DotFirst) values('al.', false);
insert into abbreviations(Value, DotFirst) values('pp.', false);
insert into abbreviations(Value, DotFirst) values('aa.', false);
insert into abbreviations(Value, DotFirst) values('ant.', false);
insert into abbreviations(Value, DotFirst) values('bac.', false);
insert into abbreviations(Value, DotFirst) values('bact.', false);
insert into abbreviations(Value, DotFirst) values('bb.', false);
insert into abbreviations(Value, DotFirst) values('dext.', false);
insert into abbreviations(Value, DotFirst) values('ext.', false);
insert into abbreviations(Value, DotFirst) values('ff.', false);
insert into abbreviations(Value, DotFirst) values('inf.', false);
insert into abbreviations(Value, DotFirst) values('int.', false);
insert into abbreviations(Value, DotFirst) values('lat.', false);
insert into abbreviations(Value, DotFirst) values('lig.', false);
insert into abbreviations(Value, DotFirst) values('ligg.', false);
insert into abbreviations(Value, DotFirst) values('med.', false);
insert into abbreviations(Value, DotFirst) values('mm.', false);
insert into abbreviations(Value, DotFirst) values('nn.', false);
insert into abbreviations(Value, DotFirst) values('post.', false);
insert into abbreviations(Value, DotFirst) values('rr.', false);
insert into abbreviations(Value, DotFirst) values('sin.', false);
insert into abbreviations(Value, DotFirst) values('sup.', false);
insert into abbreviations(Value, DotFirst) values('vag.', false);
insert into abbreviations(Value, DotFirst) values('vagg.', false);
insert into abbreviations(Value, DotFirst) values('vv.', false);
insert into abbreviations(Value, DotFirst) values('A.', false);
insert into abbreviations(Value, DotFirst) values('B.', false);
insert into abbreviations(Value, DotFirst) values('C.', false);
insert into abbreviations(Value, DotFirst) values('D.', false);
insert into abbreviations(Value, DotFirst) values('E.', false);
insert into abbreviations(Value, DotFirst) values('F.', false);
insert into abbreviations(Value, DotFirst) values('G.', false);
insert into abbreviations(Value, DotFirst) values('H.', false);
insert into abbreviations(Value, DotFirst) values('I.', false);
insert into abbreviations(Value, DotFirst) values('J.', false);
insert into abbreviations(Value, DotFirst) values('K.', false);
insert into abbreviations(Value, DotFirst) values('L.', false);
insert into abbreviations(Value, DotFirst) values('M.', false);
insert into abbreviations(Value, DotFirst) values('N.', false);
insert into abbreviations(Value, DotFirst) values('O.', false);
insert into abbreviations(Value, DotFirst) values('P.', false);
insert into abbreviations(Value, DotFirst) values('Q.', false);
insert into abbreviations(Value, DotFirst) values('R.', false);
insert into abbreviations(Value, DotFirst) values('S.', false);
insert into abbreviations(Value, DotFirst) values('T.', false);
insert into abbreviations(Value, DotFirst) values('U.', false);
insert into abbreviations(Value, DotFirst) values('V.', false);
insert into abbreviations(Value, DotFirst) values('W.', false);
insert into abbreviations(Value, DotFirst) values('X.', false);
insert into abbreviations(Value, DotFirst) values('Y.', false);
insert into abbreviations(Value, DotFirst) values('Z.', false);
insert into abbreviations(Value, DotFirst) values('.com', true);
insert into abbreviations(Value, DotFirst) values('.ru', true);
insert into abbreviations(Value, DotFirst) values('.ua', true);
insert into abbreviations(Value, DotFirst) values('.pp', true);
insert into abbreviations(Value, DotFirst) values('.net', true);
insert into abbreviations(Value, DotFirst) values('.biz', true);
insert into abbreviations(Value, DotFirst) values('.edu', true);
insert into abbreviations(Value, DotFirst) values('.gov', true);
insert into abbreviations(Value, DotFirst) values('.info', true);
insert into abbreviations(Value, DotFirst) values('.int', true);
insert into abbreviations(Value, DotFirst) values('.mobi', true);
insert into abbreviations(Value, DotFirst) values('.org', true);
insert into abbreviations(Value, DotFirst) values('.travel', true);
insert into abbreviations(Value, DotFirst) values('.xxx', true);




ALTER TABLE abbreviations DROP INDEX Value_UNIQUE;
update abbreviations set Value=REPLACE(Value, '.', '');
ALTER TABLE abbreviations ADD CONSTRAINT Value_DotFirst_UNIQUE UNIQUE (Value,DotFirst);



CREATE TABLE `antiplagiat`.`substitutions` (
  `Id` INT NOT NULL AUTO_INCREMENT,
  `Cyrillic` VARCHAR(45) NULL,
  `Latin` VARCHAR(45) NULL,
  PRIMARY KEY (`Id`));


insert into substitutions(Latin, Cyrillic) values('A','А');
insert into substitutions(Latin, Cyrillic) values('B','В');
insert into substitutions(Latin, Cyrillic) values('C','С');
insert into substitutions(Latin, Cyrillic) values('E','Е');
insert into substitutions(Latin, Cyrillic) values('H','Н');
insert into substitutions(Latin, Cyrillic) values('K','К');
insert into substitutions(Latin, Cyrillic) values('M','М');
insert into substitutions(Latin, Cyrillic) values('O','О');
insert into substitutions(Latin, Cyrillic) values('P','Р');
insert into substitutions(Latin, Cyrillic) values('T','Т');
insert into substitutions(Latin, Cyrillic) values('X','Х');
insert into substitutions(Latin, Cyrillic) values('a','а');
insert into substitutions(Latin, Cyrillic) values('c','с');
insert into substitutions(Latin, Cyrillic) values('e','е');
insert into substitutions(Latin, Cyrillic) values('i','і');
insert into substitutions(Latin, Cyrillic) values('n','п');
insert into substitutions(Latin, Cyrillic) values('o','о');
insert into substitutions(Latin, Cyrillic) values('p','р');
insert into substitutions(Latin, Cyrillic) values('y','у');
