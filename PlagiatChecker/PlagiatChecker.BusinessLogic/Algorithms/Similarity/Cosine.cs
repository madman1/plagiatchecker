﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlagiatChecker.BusinessLogic.Algorithms
{
    /// <summary>
    /// Implements Cosine Similarity between strings.The strings are first
    /// transformed in vectors of occurrences of k-shingles(sequences of k
    /// characters). In this n-dimensional space, the similarity between the two
    /// strings is the cosine of their respective vectors.
    /// </summary>
    public class Cosine : ISimilarityDetector
    {
        public double Similarity(IDictionary<string, int> profile1, IDictionary<string, int> profile2)
            => DotProduct(profile1, profile2) / (Norm(profile1) * Norm(profile2));

        public double Distance(IDictionary<string, int> profile1, IDictionary<string, int> profile2)
            => 1.0 - Similarity(profile1, profile2);

        /// <summary>
        /// Compute L2 norm: sqrt(Sum_i( v_i²))
        /// </summary>
        private static double Norm(IDictionary<string, int> profile)
        {
            double agg = 0;

            foreach (var entry in profile)
            {
                agg += 1.0 * entry.Value * entry.Value;
            }

            return Math.Sqrt(agg);
        }

        private static double DotProduct(IDictionary<string, int> profile1,
            IDictionary<string, int> profile2)
        {
            // Loop over the smallest map
            var small_profile = profile2;
            var large_profile = profile1;

            if (profile1.Count < profile2.Count)
            {
                small_profile = profile1;
                large_profile = profile2;
            }

            double agg = 0;
            foreach (var entry in small_profile)
            {
                int i;

                if (!large_profile.TryGetValue(entry.Key, out i)) continue;

                agg += 1.0 * entry.Value * i;
            }

            return agg;
        }
    }
}
