﻿using System.Collections.Generic;
using System.Linq;

namespace PlagiatChecker.BusinessLogic.Algorithms
{
    /// <summary>
    /// Computes jaccard similarity: |A inter B| / |A union B|.
    /// </summary>
    public class Jaccard : ISimilarityDetector
    {
        public double Similarity(IDictionary<string, int> profile1, IDictionary<string, int> profile2)
        {
            var union = new HashSet<string>();
            union.UnionWith(profile1.Keys);
            union.UnionWith(profile2.Keys);

            int inter = 0;

            foreach (var key in union)
            {
                if (profile1.ContainsKey(key) && profile2.ContainsKey(key))
                    inter++;
            }

            return 1.0 * inter / union.Count;
        }

        public static double Similarity<T>(IEnumerable<T> ls1, IEnumerable<T> ls2)
        {
            var hs1 = new HashSet<T>(ls1);
            var hs2 = new HashSet<T>(ls2);
            return ((double)hs1.Intersect(hs2).Count() / (double)hs1.Union(hs2).Count());
        }
    }
}
