﻿using System;
using System.Collections.Generic;

namespace PlagiatChecker.BusinessLogic.Algorithms
{
    /// <summary>
    /// Sorensen-Dice coefficient, aka Sørensen index, Dice's coefficient or
    /// Czekanowski's binary (non-quantitative) index.
    ///
    /// The similarity is computed as 2 * |A inter B| / (|A| + |B|)
    /// Attention: Sorensen-Dice distance (and similarity) does not
    /// satisfy triangle inequality.
    /// </summary>
    public class SorensenDice : ISimilarityDetector
    {
        public double Similarity(IDictionary<string, int> profile1, IDictionary<string, int> profile2)
        {
            var union = new HashSet<string>();
            union.UnionWith(profile1.Keys);
            union.UnionWith(profile2.Keys);

            int inter = 0;

            foreach (var key in union)
            {
                if (profile1.ContainsKey(key) && profile2.ContainsKey(key))
                    inter++;
            }

            return 2.0 * inter / (profile1.Count + profile2.Count);
        }

        public double Distance(IDictionary<string, int> profile1, IDictionary<string, int> profile2)
            => 1 - Similarity(profile1, profile2);
    }
}
