﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PlagiatChecker.BusinessLogic.Algorithms
{
    /// The Jaro–Winkler distance metric is designed and best suited for short
    /// strings such as person names, and to detect typos; it is (roughly) a
    /// variation of Damerau-Levenshtein, where the substitution of 2 close
    /// characters is considered less important then the substitution of 2 characters
    /// that a far from each other.
    /// Jaro-Winkler was developed in the area of record linkage (duplicate
    /// detection) (Winkler, 1990). It returns a value in the interval [0.0, 1.0].
    /// The distance is computed as 1 - Jaro-Winkler similarity.
    public class JaroWinkler : ISimilarityDetector
    {
        private static readonly double DEFAULT_THRESHOLD = 0.7;
        private static readonly int THREE = 3;
        private static readonly double JW_COEF = 0.1;

        /// <summary>
        /// The current value of the threshold used for adding the Winkler bonus. The default value is 0.7.
        /// </summary>
        private double Threshold { get; }

        /// <summary>
        /// Creates a new instance with default threshold (0.7)
        /// </summary>
        public JaroWinkler()
        {
            Threshold = DEFAULT_THRESHOLD;
        }

        /// <summary>
        /// Creates a new instance with given threshold to determine when Winkler bonus should
        /// be used. Set threshold to a negative value to get the Jaro distance.
        /// </summary>
        /// <param name="threshold"></param>
        public JaroWinkler(double threshold)
        {
            Threshold = threshold;
        }

        public double Similarity(IDictionary<string, int> profile1, IDictionary<string, int> profile2)
        {            
            if (string.Equals(profile1, profile2)) return 1f;

            string[] s1Words = profile1.Keys.ToArray();
            string[] s2Words = profile2.Keys.ToArray();

            int[] mtp = Matches(s1Words, s2Words);
            float m = mtp[0];
            if (m == 0)
            {
                return 0f;
            }
            double j = ((m / s1Words.Length + m / s2Words.Length + (m - mtp[1]) / m))
                    / THREE;
            double jw = j;

            if (j > Threshold)
            {
                jw = j + Math.Min(JW_COEF, 1.0 / mtp[THREE]) * mtp[2] * (1 - j);
            }
            return jw;
        }

        public double Distance(IDictionary<string, int> profile1, IDictionary<string, int> profile2)
            => 1.0 - Similarity(profile1, profile2);

        private int[] Matches(string[] s1, string[] s2)
        {
            string[] max, min;
            if (s1.Length > s2.Length)
            {
                max = s1;
                min = s2;
            }
            else
            {
                max = s2;
                min = s1;
            }
            int range = Math.Max(max.Length / 2 - 1, 0);

            //int[] matchIndexes = new int[min.Length];
            //Arrays.fill(matchIndexes, -1);
            int[] matchIndexes = Enumerable.Repeat(-1, min.Length).ToArray();

            bool[] matchFlags = new bool[max.Length];
            int matches = 0;
            for (int mi = 0; mi < min.Length; mi++)
            {
                string c1 = min[mi];
                for (int xi = Math.Max(mi - range, 0),
                        xn = Math.Min(mi + range + 1, max.Length); xi < xn; xi++)
                {
                    if (!matchFlags[xi] && c1 == max[xi])
                    {
                        matchIndexes[mi] = xi;
                        matchFlags[xi] = true;
                        matches++;
                        break;
                    }
                }
            }
            string[] ms1 = new string[matches];
            string[] ms2 = new string[matches];
            for (int i = 0, si = 0; i < min.Length; i++)
            {
                if (matchIndexes[i] != -1)
                {
                    ms1[si] = min[i];
                    si++;
                }
            }
            for (int i = 0, si = 0; i < max.Length; i++)
            {
                if (matchFlags[i])
                {
                    ms2[si] = max[i];
                    si++;
                }
            }
            int transpositions = 0;
            for (int mi = 0; mi < ms1.Length; mi++)
            {
                if (ms1[mi] != ms2[mi])
                {
                    transpositions++;
                }
            }
            int prefix = 0;
            for (int mi = 0; mi < min.Length; mi++)
            {
                if (s1[mi] == s2[mi])
                {
                    prefix++;
                }
                else
                {
                    break;
                }
            }
            return new int[] { matches, transpositions / 2, prefix, max.Length };
        }
    }
}
