﻿using PlagiatChecker.BusinessLogic.Hashing;
using PlagiatChecker.BusinessLogic.Logic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace PlagiatChecker.BusinessLogic.Algorithms
{
    public class ShinglesService
    {
        private const bool WORDS_TO_LOWER = true;
        private const int SHINGLE_LENGTH = 6;
        private readonly IHashingService _hashService;

        public ShinglesService(IHashingService hashService)
        {
            _hashService = hashService;
        }

        public string[] GetShingles(string source, int shingleLenght = SHINGLE_LENGTH)
        {
            var stemWords = source.GetWords(WORDS_TO_LOWER).GetWordsStemmers();
            string[] shingles = SplitWordsOnShingles(stemWords, shingleLenght);
            var hashedShingles = shingles.ForEach(x => _hashService.CalculateHash(x)).ToArray();

            return hashedShingles;
        }

        public int[] GetNumericShingles(string source, int shingleLenght = SHINGLE_LENGTH)
        {
            var stemWords = source.GetWords(WORDS_TO_LOWER).GetWordsStemmers();
            string[] shingles = SplitWordsOnShingles(stemWords, shingleLenght);
            var hashedShingles = shingles.ForEach(x => _hashService.CalculateHashNumber(x)).ToArray();

            return hashedShingles;
        }

        /// <summary>
        /// Get hashed shingles with the number of occurances 
        /// </summary>
        public IDictionary<string, int> GetProfile(string source, int shingleLenght = SHINGLE_LENGTH)
        {
            var stemWords = source.GetWords(WORDS_TO_LOWER).GetWordsStemmers();
            var shingles = SplitWordsOnShingles(stemWords, shingleLenght);

            var result = new Dictionary<string, int>();

            foreach(var shingle in shingles)
            {
                //var shingle = _hashService.CalculateHash(plainShingle);

                if (result.TryGetValue(shingle, out int old))
                {
                    result[shingle] = old + 1;
                }
                else
                {
                    result[shingle] = 1;
                }
            }

            return new ReadOnlyDictionary<string, int>(result);
        }

        public Dictionary<string, List<int>> GetShingleToWordIndexes(string source, int shingleLenght = SHINGLE_LENGTH)
        {
            var stemWords = source.GetWords(WORDS_TO_LOWER).GetWordsStemmers();
            var shingles = SplitWordsOnShingles(stemWords, shingleLenght);

            var result = new Dictionary<string, List<int>>();
            for (int i = 0; i < shingles.Length; i++)
            {
                //var shingle = _hashService.CalculateHash(shingles[i]);
                var shingle = shingles[i];

                if (!result.ContainsKey(shingle))
                {
                    result[shingle] = new List<int>();
                }

                result[shingle].AddRange(Enumerable.Range(i, shingleLenght));
            }

            return result;
        }

        private string[] SplitWordsOnShingles(IEnumerable<string> words, int shingleLength)
        {
            if (words == null)
            {
                throw new ArgumentException("words");
            }

            string[] wordsArray;
            if (words is string[])
            {
                wordsArray = (string[])words;
            }
            else
            {
                wordsArray = words.ToArray();
            }

            if (shingleLength > wordsArray.Length)
            {
                return new string[] { string.Join(" ", wordsArray) };
            }

            int shinglesCount = wordsArray.Length - shingleLength + 1;
            var result = new string[shinglesCount];

            for (int i = 0; i < shinglesCount; i++)
            {
                string[] innerArray = new string[shingleLength];
                Array.Copy(wordsArray, i, innerArray, 0, shingleLength);
                result[i] = string.Join(" ", innerArray);
            }

            return result;
        }
    }
}
