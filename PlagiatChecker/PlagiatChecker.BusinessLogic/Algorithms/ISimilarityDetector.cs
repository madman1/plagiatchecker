﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlagiatChecker.BusinessLogic.Algorithms
{
    public interface ISimilarityDetector
    {
        double Similarity(IDictionary<string, int> profile1, IDictionary<string, int> profile2);
    }
}
