﻿using System;
using System.Collections.Generic;

namespace PlagiatChecker.BusinessLogic.Algorithms.Fingerprinting
{
    public class SimHash
    {
        private const int HashSize = 32;

        public float GetSimilarity(IEnumerable<int> wordIds, IEnumerable<int> wordCopyIds)
        {
            var needleSimHash = this.DoCalculateSimHash(wordIds);
            var hayStackSimHash = this.DoCalculateSimHash(wordCopyIds);
            return (HashSize - GetHammingDistance(needleSimHash, hayStackSimHash)) / (float)HashSize;
        }

        #region Private Methods

        private static int GetHammingDistance(int firstValue, int secondValue)
        {
            var hammingBits = firstValue ^ secondValue;
            var hammingValue = 0;
            for (int i = 0; i < 32; i++)
            {
                if (IsBitSet(hammingBits, i))
                {
                    hammingValue += 1;
                }
            }
            return hammingValue;
        }

        private static bool IsBitSet(int b, int pos)
        {
            return (b & (1 << pos)) != 0;
        }

        private int DoCalculateSimHash(IEnumerable<int> wordIds)
        {
            var vector = new int[HashSize];
            for (var i = 0; i < HashSize; i++)
            {
                vector[i] = 0;
            }

            foreach (var value in wordIds)
            {
                for (var j = 0; j < HashSize; j++)
                {
                    if (IsBitSet(value, j))
                    {
                        vector[j] += 1;
                    }
                    else
                    {
                        vector[j] -= 1;
                    }
                }
            }

            var fingerprint = 0;
            for (var i = 0; i < HashSize; i++)
            {
                if (vector[i] > 0)
                {
                    fingerprint += 1 << i;
                }
            }
            return fingerprint;
        }

        #endregion
    }
}
