﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace PlagiatChecker.BusinessLogic.Algorithms.Fingerprinting
{
    public class MinHash
    {
        private const int NUMBER_OF_HASH_FUNCTIONS = 100;
        private Hash[] hashFunctions;

        public int UniverseSize { get; set; }

        public MinHash(int universeSize = 500)
        {
            UniverseSize = universeSize;
            // number of bits to store the universe
            int u = BitsForUniverse(universeSize);
            GenerateHashFunctions(u);
        }

        public delegate int Hash(int toHash);

        public List<int> GetMinHashes(IEnumerable<int> wordIds)
        {
            int[] minHashes = new int[NUMBER_OF_HASH_FUNCTIONS];
            for (int h = 0; h < NUMBER_OF_HASH_FUNCTIONS; h++)
            {
                minHashes[h] = int.MaxValue;
            }
            foreach (int id in wordIds)
            {
                for (int h = 0; h < NUMBER_OF_HASH_FUNCTIONS; h++)
                {
                    int hash = hashFunctions[h](id);
                    minHashes[h] = Math.Min(minHashes[h], hash);
                }
            }
            return minHashes.ToList();
        }

        #region Private Methods 

        // Generates the Universal Random Hash functions
        // http://en.wikipedia.org/wiki/Universal_hashing
        private void GenerateHashFunctions(int u)
        {
            hashFunctions = new Hash[NUMBER_OF_HASH_FUNCTIONS];

            Random r = new Random(11);
            for (int i = 0; i < NUMBER_OF_HASH_FUNCTIONS; i++)
            {
                uint a = (uint)r.Next(u);
                uint b = (uint)r.Next(u);
                uint c = (uint)r.Next(u);
                hashFunctions[i] = x => QHash(x, a, b, c);
            }
        }

        private int BitsForUniverse(int universeSize)
        {
            return (int)Math.Truncate(Math.Log((double)universeSize, 2.0)) + 1;
        }

        private static int QHash(int x, uint a, uint b, uint c)
        {
            int hashValue = (int)((a * (x >> 4) + b * x + c) & 131071);
            return Math.Abs(hashValue);
        }

        #endregion
    }
}
