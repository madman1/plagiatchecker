﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using PlagiatChecker.BusinessLogic.Algorithms;
using PlagiatChecker.BusinessLogic.Algorithms.Fingerprinting;
using PlagiatChecker.BusinessLogic.Hashing;
using PlagiatChecker.BusinessLogic.HelpFiles;
using PlagiatChecker.BusinessLogic.Logic;

namespace PlagiatChecker.BusinessLogic
{
    public class BusinessLogicParams
    {
        public bool ProductionEnvironment;
        public bool YandexTurnOn { get; set; }
        public string YandexKey { get; set; }
        public string YandexUser { get; set; }
        public string YandexApiUri { get; set; }
    }
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection InitialiseBusinessLogicDependencies(this IServiceCollection serviceCollection, BusinessLogicParams parameters)
        {
            serviceCollection.AddScoped<IHashingService, Sha256HashService>();
            serviceCollection.AddScoped<ShinglesService>();

            serviceCollection.AddScoped<Cosine>();
            serviceCollection.AddScoped<Jaccard>();
            serviceCollection.AddScoped<JaroWinkler>();
            serviceCollection.AddScoped<SorensenDice>();

            serviceCollection.AddScoped<MinHash>();
            serviceCollection.AddScoped<SimHash>();

            if (parameters.YandexTurnOn)
            {
                serviceCollection.AddScoped<IInternetSearchApi>(x => new InternetSearchApi(
                    parameters.YandexKey,
                    parameters.YandexUser,
                    parameters.YandexApiUri,
                    x.GetService<ILogger<IInternetSearchApi>>()));
            }
            else
            {
                serviceCollection.AddScoped<IInternetSearchApi, InternetSearchApiStub>();
            }

            serviceCollection.AddScoped<AlgorithmsFactory>();
            serviceCollection.AddScoped<HtmlParser>();
            serviceCollection.AddScoped<ITextHandler, TextHandler>();
            serviceCollection.AddScoped<TextComparator>();

            serviceCollection.AddSingleton<StatusManager>();

            return serviceCollection;
        }
    }
}
