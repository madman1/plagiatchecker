﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Text;

namespace PlagiatChecker.BusinessLogic.Hashing
{
    public class Sha256HashService : IHashingService
    {
        public string CalculateHash(string input)
        {
            // step 1, calculate MD5 hash from input
            SHA256 shaHash = SHA256.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = shaHash.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public int CalculateHashNumber(string input)
        {
            SHA256 shaHash = SHA256.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = shaHash.ComputeHash(inputBytes);

            int convertedHash = BitConverter.ToInt32(hash, 0);
            return convertedHash;
        }
    }
}
