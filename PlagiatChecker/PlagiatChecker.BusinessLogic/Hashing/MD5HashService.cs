﻿using System;
using System.Security.Cryptography;
using System.Text;

namespace PlagiatChecker.BusinessLogic.Hashing
{
    public class MD5HashService : IHashingService
    {
        public string CalculateHash(string input)
        {
            // step 1, calculate MD5 hash from input
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            // step 2, convert byte array to hex string
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < hash.Length; i++)
            {
                sb.Append(hash[i].ToString("X2"));
            }
            return sb.ToString();
        }

        public int CalculateHashNumber(string input)
        {
            MD5 md5 = MD5.Create();
            byte[] inputBytes = Encoding.ASCII.GetBytes(input);
            byte[] hash = md5.ComputeHash(inputBytes);

            int convertedHash = BitConverter.ToInt32(hash, 0);
            return convertedHash;
        }
    }
}
