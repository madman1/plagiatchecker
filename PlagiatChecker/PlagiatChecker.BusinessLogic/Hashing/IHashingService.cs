﻿namespace PlagiatChecker.BusinessLogic.Hashing
{
    public interface IHashingService
    {
        string CalculateHash(string input);
        int CalculateHashNumber(string input);
    }
}
