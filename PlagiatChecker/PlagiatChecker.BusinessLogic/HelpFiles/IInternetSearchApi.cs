﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlagiatChecker.BusinessLogic.HelpFiles
{
    public interface IInternetSearchApi
    {
        Task<IEnumerable<string>> FindSitesAsync(string[] words, int maxForComparison = 15, int shingleLenght = 5);
    }
}
