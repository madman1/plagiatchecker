﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using HtmlAgilityPack;
using System.Threading.Tasks;
using System.Linq;

namespace PlagiatChecker.BusinessLogic.HelpFiles
{
    public class HtmlParser
    {
        public async Task<string> GetTextOfSiteAsync(string url)
        {
            if (url.EndsWith(".pdf") || url.EndsWith(".djvu"))
            {
                return null;
            }

            using (HttpClient http = new HttpClient())
            using (HttpResponseMessage response = await http.GetAsync(url).ConfigureAwait(false))
            using (HttpContent content = response.Content)
            {
                var source = await content.ReadAsStringAsync().ConfigureAwait(false);

                try
                {
                    return await GetPlainTextFromHtml(source, content).ConfigureAwait(false);
                }
                catch (KeyNotFoundException)
                {
                    return null;
                }
            }
        }

        #region Private Methods

        public async Task<string> GetPlainTextFromHtml(string html, HttpContent content)
        {
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(html);

            HtmlNode metaTag = htmlDoc.DocumentNode.SelectSingleNode("//meta");
            if (metaTag != null)
            {
                HtmlAttribute attrCharset = metaTag.Attributes["charset"];
                HtmlAttribute attrContent = metaTag.Attributes["content"];
                
                if (attrCharset != null && !string.Equals(attrCharset.Value, "UTF-8", StringComparison.OrdinalIgnoreCase))
                {
                    var htmlInBytes = await content.ReadAsByteArrayAsync().ConfigureAwait(false);
                    string source = Encoding.GetEncoding(attrCharset.Value).GetString(htmlInBytes, 0, htmlInBytes.Length - 1);
                    htmlDoc.LoadHtml(source);
                }
                else if (attrContent != null && (attrContent.Value.Contains("windows-1251") || attrContent.Value.Contains("WINDOWS-1251")))
                {
                    var htmlInBytes = await content.ReadAsByteArrayAsync().ConfigureAwait(false);
                    string source = Encoding.GetEncoding("windows-1251").GetString(htmlInBytes, 0, htmlInBytes.Length - 1);
                    htmlDoc.LoadHtml(source);
                }
            }

            StringBuilder result = new StringBuilder();
            try
            {
                ConvertToLinear(htmlDoc.DocumentNode, result);
            }
            catch
            {
                return result.ToString().Trim();
            }
            return result.ToString().Trim();
        }

        #region Loop

        private void ConvertToLinear(HtmlNode node, StringBuilder outText)
        {
            Stack<HtmlNode> _nodes = new Stack<HtmlNode>();

            _nodes.Push(node);

            while (_nodes.Count > 0)
            {
                node = _nodes.Pop();

                string html;
                switch (node.NodeType)
                {
                    case HtmlNodeType.Comment:
                        // don't output comments
                        break;

                    case HtmlNodeType.Document:
                        foreach (var child in node.ChildNodes)
                        {
                            _nodes.Push(child);
                        }
                        break;

                    case HtmlNodeType.Text:
                        // script and style must not be output
                        string parentName = node.ParentNode.Name;
                        if ((parentName == "script") || (parentName == "style"))
                            break;

                        // get text
                        if (parentName == "p" ||
                            parentName == "div" ||
                            parentName == "article" ||
                            parentName == "span" ||
                            parentName == "section" ||
                            parentName == "header" ||
                            parentName == "footer" ||
                            parentName == "b" ||
                            parentName == "h1" ||
                            parentName == "h2" ||
                            parentName == "h3" ||
                            parentName == "strong"
                            )
                        {
                            html = GetNodeText((HtmlTextNode)node);
                            if (!string.IsNullOrEmpty(html))
                            {
                                outText.Append(html);
                            }
                        }
                        else if (parentName == "a")
                        {
                            html = GetNodeText((HtmlTextNode)node);

                            if (!string.IsNullOrEmpty(html) &&
                                !Uri.IsWellFormedUriString(html, UriKind.RelativeOrAbsolute))
                            {
                                outText.Append(html);
                            }
                        }
                        break;

                    case HtmlNodeType.Element:
                        switch (node.Name)
                        {
                            case "p":
                                // treat paragraphs as crlf
                                outText.Append("\r\n");
                                break;
                        }

                        if (node.HasChildNodes)
                        {
                            foreach (var child in node.ChildNodes.Reverse())
                            {
                                _nodes.Push(child);
                            }
                        }
                        break;
                }
            }
        }

        private string GetNodeText(HtmlTextNode node)
        {
            var html = node.Text;

            // is it in fact a special closing node output as text?
            if (HtmlNode.IsOverlappedClosingElement(html))
                return null;

            // check the text is meaningful and not a bunch of whitespaces
            if (html.Trim().Length > 0)
            {
                return HtmlEntity.DeEntitize(html);
            }

            return null;
        }

        private string GetNodeText(string text)
        {
            // is it in fact a special closing node output as text?
            if (HtmlNode.IsOverlappedClosingElement(text))
                return null;

            // check the text is meaningful and not a bunch of whitespaces
            if (text.Trim().Length > 0)
            {
                return HtmlEntity.DeEntitize(text);
            }

            return null;
        }

        #endregion

        #region Recursive

        private void ConvertContentTo(HtmlNode node, StringBuilder outText)
        {
            foreach (HtmlNode subnode in node.ChildNodes)
            {
                ConvertTo(subnode, outText);
            }
        }

        private void ConvertTo(HtmlNode node, StringBuilder outText)
        {
            string html;
            switch (node.NodeType)
            {
                case HtmlNodeType.Comment:
                    // don't output comments
                    break;

                case HtmlNodeType.Document:
                    ConvertContentTo(node, outText);
                    break;

                case HtmlNodeType.Text:
                    // script and style must not be output
                    string parentName = node.ParentNode.Name;
                    if ((parentName == "script") || (parentName == "style"))
                        break;

                    // get text
                    if (node.ParentNode.Name == "p" ||
                        node.ParentNode.Name == "article")
                    {
                        html = ((HtmlTextNode)node).Text;

                        // is it in fact a special closing node output as text?
                        if (HtmlNode.IsOverlappedClosingElement(html))
                            break;

                        // check the text is meaningful and not a bunch of whitespaces
                        if (html.Trim().Length > 0)
                        {
                            outText.Append(HtmlEntity.DeEntitize(html));
                        }
                    }
                    break;

                case HtmlNodeType.Element:
                    switch (node.Name)
                    {
                        case "p":
                            // treat paragraphs as crlf
                            outText.Append("\r\n");
                            break;
                    }

                    if (node.HasChildNodes)
                    {
                        ConvertContentTo(node, outText);
                    }
                    break;
            }
        }

        #endregion

        #endregion
    }
}
