﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlagiatChecker.BusinessLogic.HelpFiles
{
    public class InternetSearchApiStub : IInternetSearchApi
    {
        public Task<IEnumerable<string>> FindSitesAsync(string[] words, int maxForComparison = 15, int shingleLenght = 5)
        {

            IEnumerable<string> sites = new string[] {

                // newton
                "https://ru.wikipedia.org/wiki/%D0%97%D0%B0%D0%BA%D0%BE%D0%BD%D1%8B_%D0%9D%D1%8C%D1%8E%D1%82%D0%BE%D0%BD%D0%B0",

                //ukraine
                //"https://uk.wikipedia.org/wiki/%D0%A3%D0%BA%D1%80%D0%B0%D1%97%D0%BD%D0%B0",

                //"http://search.ligazakon.ua/l_doc2.nsf/link1/T070537.html",
                //"https://uk.wiki.ng/wiki/%D0%9E%D0%B1%D1%80%D0%BE%D0%B1%D0%BA%D0%B0_%D0%BF%D1%80%D0%B8%D1%80%D0%BE%D0%B4%D0%BD%D0%BE%D1%97_%D0%BC%D0%BE%D0%B2%D0%B8",

                 // diploma text
                //"http://www.zirozebar.com/pedia-uk/wiki/%D0%9E%D0%B1%D1%80%D0%BE%D0%B1%D0%BA%D0%B0_%D0%BF%D1%80%D0%B8%D1%80%D0%BE%D0%B4%D0%BD%D0%BE%D1%97_%D0%BC%D0%BE%D0%B2%D0%B8",
                //"http://www.gpedia.com/uk/gpedia/%D0%9E%D0%B1%D1%80%D0%BE%D0%B1%D0%BA%D0%B0_%D0%BF%D1%80%D0%B8%D1%80%D0%BE%D0%B4%D0%BD%D0%BE%D1%97_%D0%BC%D0%BE%D0%B2%D0%B8",
                //"https://uk.wikipedia.org/wiki/%D0%9E%D0%B1%D1%80%D0%BE%D0%B1%D0%BA%D0%B0_%D0%BF%D1%80%D0%B8%D1%80%D0%BE%D0%B4%D0%BD%D0%BE%D1%97_%D0%BC%D0%BE%D0%B2%D0%B8",
                //"https://uk.wiki.ng/wiki/%D0%9E%D0%B1%D1%80%D0%BE%D0%B1%D0%BA%D0%B0_%D0%BF%D1%80%D0%B8%D1%80%D0%BE%D0%B4%D0%BD%D0%BE%D1%97_%D0%BC%D0%BE%D0%B2%D0%B8",
                //"http://uk.sansursuzwikipedia.org/wiki/%D0%9E%D0%B1%D1%80%D0%BE%D0%B1%D0%BA%D0%B0_%D0%BF%D1%80%D0%B8%D1%80%D0%BE%D0%B4%D0%BD%D0%BE%D1%97_%D0%BC%D0%BE%D0%B2%D0%B8",
                //"http://www.nidiot.de/uk/%D0%9E%D0%B1%D1%80%D0%BE%D0%B1%D0%BA%D0%B0_%D0%BF%D1%80%D0%B8%D1%80%D0%BE%D0%B4%D0%BD%D0%BE%D1%97_%D0%BC%D0%BE%D0%B2%D0%B8",
                //"http://znaimo.com.ua/%D0%9E%D0%B1%D1%80%D0%BE%D0%B1%D0%BA%D0%B0_%D0%BF%D1%80%D0%B8%D1%80%D0%BE%D0%B4%D0%BD%D0%BE%D1%97_%D0%BC%D0%BE%D0%B2%D0%B8",
                //"https://www.wikiplanet.click/enciclopedia/uk/%D0%9E%D0%B1%D1%80%D0%BE%D0%B1%D0%BA%D0%B0_%D0%BF%D1%80%D0%B8%D1%80%D0%BE%D0%B4%D0%BD%D0%BE%D1%97_%D0%BC%D0%BE%D0%B2%D0%B8",
                //"https://StudFiles.net/preview/2300132/",
                //"http://ua-referat.com/%D0%86%D0%BD%D1%84%D0%BE%D1%80%D0%BC%D0%B0%D1%86%D1%96%D0%B9%D0%BD%D1%96_%D0%BF%D0%BE%D1%82%D1%80%D0%B5%D0%B1%D0%B8_%D1%82%D0%B0_%D1%96%D0%BD%D1%84%D0%BE%D1%80%D0%BC%D0%B0%D1%86%D1%96%D0%B9%D0%BD%D1%96_%D0%B7%D0%B0%D0%BF%D0%B8%D1%82%D0%B8"

                // versal dogovor
                //"http://studbooks.net/608438/istoriya/territorii",
                //"http://www.encyclopaedia-russia.ru/article.php?id=175",
                //"http://konservatizm.org/seminars/sotsiologiya-geopoliticheskih-protsessov/030110085555.xhtml",
                //"http://dic.academic.ru/dic.nsf/ruwiki/754233"

                //"http://risovach.ru/kartinka/11156690",
                //"http://androidmafia.ru/video/2YsPYa5DRVA",
                //"http://hmongbuy.net/video/2YsPYa5DRVA",
                //"http://slovoedgame.ru/v/v2YsiPYad5DReVAo/plennyj_letchik_major_antonov_istoriya_odnoj_fotografii.html",
                //"http://topdownloads.ru/watch/2YsPYa5DRVA.htm",
                //"http://video.fraia-kino.ru/watch/-fOVsnqoalc",
                //"http://lektsii.org/15-15507.html",
                //"http://thenerdcabinet.com/watch/2YsPYa5DRVA",
                //"http://helpiks.org/6-39097.html",
                //"http://mashav.ucoz.net/index/predposylki_vojny/0-36",
                //"http://novoe-video.info/video/2YsPYa5DRVA/",
                //"http://ru.woyna.wikia.com/wiki/Вторая_мировая_война",
                //"http://seabattle.school-8.com/Page10/SWW.htm",
                //"http://vladsherbani.narod.ru/index/0-2",
                //"http://ru.wiguru.org/info/Вторая_мировая_война",
                //"http://studwood.ru/1259849/istoriya/predposylki_voyny_evr...posylki_voyny_evrope",
                //"http://revolution.allbest.ru/history/00345431_0.html",
                //"http://erp.harsen.co.id/projects/kogda-nachalas-i-zakonch...-mirovaya-voyna.html",
                //"http://polandinfo.ru/Container/Details/997",
            };

            return Task.FromResult(sites.Take(maxForComparison));
        }
    }
}
