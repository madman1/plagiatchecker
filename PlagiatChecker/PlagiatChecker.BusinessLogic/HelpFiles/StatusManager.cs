﻿using System;
using System.Collections.Concurrent;

namespace PlagiatChecker.BusinessLogic.HelpFiles
{
    public enum RequestStatus
    {
        Processing,
        InternetSearch,
        Comparing, 
        FinalCalculations
    }

    public class StatusManager
    {
        private static ConcurrentDictionary<string, int> _keyToStatus = new ConcurrentDictionary<string, int>();

        public void AddOrUpdate(string key, RequestStatus status)
        {
            _keyToStatus.AddOrUpdate(key, (int)status, (k, value) => (int)status);
        }

        public int GetValue(string key)
        {
            var succeed =_keyToStatus.TryGetValue(key, out int res);
            return succeed ? res : 0;
        }

        public void RemoveKey(string key)
        {
            _keyToStatus.TryRemove(key, out int temp);
        }
    }
}
