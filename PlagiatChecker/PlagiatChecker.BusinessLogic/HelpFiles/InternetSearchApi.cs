﻿using Microsoft.Extensions.Logging;
using PlagChecker.Models.Exceptions;
using PlagiatChecker.BusinessLogic.Logic;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace PlagiatChecker.BusinessLogic.HelpFiles
{
    public class InternetSearchApi : IInternetSearchApi
    {
        private readonly string yandexApiKey;
        private readonly string yandexApiUser;
        private readonly string yandexApiUri;
        private readonly ILogger<IInternetSearchApi> logger;

        public InternetSearchApi(string yandexApiKey, string yandexApiUser, string yandexApiUri, ILogger<IInternetSearchApi> logger)
        {
            this.yandexApiKey = yandexApiKey;
            this.yandexApiUser = yandexApiUser;
            this.yandexApiUri = yandexApiUri;
            this.logger = logger;
        }

        public async Task<IEnumerable<string>> FindSitesAsync(string[] words, int maxForComparison = 15, int shingleLenght = 5)
        {
            Dictionary<string, int> result = new Dictionary<string, int>();
            var wLength = words.Length;
            int skipNext = wLength > 1500 ? 5 : wLength > 500 ? 4 : wLength > 150 ? 3 : wLength > 50 ? 2 : 1;

            var tasks = new List<Task<IEnumerable<string>>>();

            int j = 1;
            for (int i = 0; i < words.Length - 1 - shingleLenght; i += shingleLenght * skipNext)
            {
                string[] innerArray = new string[shingleLenght];
                Array.Copy(words, i, innerArray, 0, shingleLenght);
                string yandexQuery = string.Join(" ", innerArray);
                tasks.Add(GetSites(yandexQuery));

                j++;
                if (j % 3 == 0)
                    Thread.Sleep(300);
            }

            try
            {
                await Task.WhenAll(tasks).ConfigureAwait(false);
            }
            catch {/*ignore*/}

            foreach (var task in tasks)
            {
                if (task.Exception != null || task.Result == null)
                {
                    continue;
                }

                var foundSites = task.Result;
                var maxPriority = foundSites.Count();
                int priority = maxPriority;
                foreach (var site in foundSites)
                {
                    if (result.ContainsKey(site))
                    {
                        result[site] += maxPriority;
                    }
                    else
                    {
                        result.Add(site, priority);
                    }

                    priority--;
                }
            }

            return result.OrderByDescending(x => x.Value).Take(maxForComparison).Select(x => x.Key).ToArray();
        }

        public async Task<IEnumerable<string>> GetSites(string searchQuery)
        {
            string url = $"{yandexApiUri}?user={yandexApiUser}&key={yandexApiKey}&query={searchQuery}&l10n=en" +
                "&sortby=rlv&filter=none&groupby=attr%3D%22%22.mode%3Dflat.groups-on-page%3D10.docs-in-group%3D1";

            Uri uri = new Uri(url);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response = null;

            try
            {
                response = (HttpWebResponse)(await request.GetResponseAsync().ConfigureAwait(false));
            }
            catch (Exception ex)
            {
                throw new BadRequestException("Failed to get sites from Yandex", ex);
            }

            XmlReader xmlReader = XmlReader.Create(response.GetResponseStream());
            XDocument xmlResponse = XDocument.Load(xmlReader);

            List<string> result = new List<string>();

            var yandexErrors = xmlResponse.Elements()
                .Elements("response")
                .Elements("error")
                .Select(x => x.Value)
                .ToList();

            if (yandexErrors.Count > 0)
            {
                yandexErrors.Apply(x => logger.LogWarning(x));
                return result;
            }

            var groupQuery = xmlResponse.Elements().
               Elements("response").
               Elements("results").
               Elements("grouping").
               Elements("group").ToList();

            string urlQuery;

            for (int i = 0; i < groupQuery.Count(); i++)
            {
                urlQuery = groupQuery.ElementAt(i).Element("doc").Element("url").Value;
                result.Add(urlQuery);
            }

            return result;
        }

    }
}
