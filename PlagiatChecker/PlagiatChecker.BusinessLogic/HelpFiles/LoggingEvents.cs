﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlagiatChecker.BusinessLogic.HelpFiles
{
    public class LoggingEvents
    {
        //Errors
        public const int GetYandexSites = 1000;
        // Warnings
        public const int FailedToGetText = 2000;
        public const int IncorrectWordsDivide = 2001;
    }
}
