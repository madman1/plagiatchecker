﻿using Microsoft.Extensions.Logging;
using PlagChecker.Models;
using PlagChecker.Models.Exceptions;
using PlagiatChecker.BusinessLogic.Algorithms;
using PlagiatChecker.BusinessLogic.HelpFiles;
using PlagiatChecker.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PlagiatChecker.BusinessLogic.Logic
{
    public class TextComparator
    {
        private readonly ITextHandler _textHandler;
        private readonly IInternetSearchApi _internetSearchApi;
        private readonly HtmlParser _htmlParser;
        private readonly AlgorithmsFactory _algorithmsFactory;
        private readonly ShinglesService _shinglesService;
        private readonly ILogger<TextComparator> _logger;
        private readonly StatusManager _statusManager;

        public TextComparator(ITextHandler textHandler,
            IInternetSearchApi internetSearchApi,
            HtmlParser htmlParser,
            AlgorithmsFactory algorithmsFactory,
            ShinglesService shinglesService,
            ILogger<TextComparator> logger,
            StatusManager statusManager)
        {
            _textHandler = textHandler;
            _internetSearchApi = internetSearchApi;
            _htmlParser = htmlParser;
            _algorithmsFactory = algorithmsFactory;
            _shinglesService = shinglesService;
            _logger = logger;
            _statusManager = statusManager;
        }

        public async Task<RequestResult> CompareWebsite(TextCheckRequest request)
        {
            var link = request.Link;
            link = link.StartsWith("http") ? link : string.Concat("http://", link);
            var websiteText = await GetTextFromUrl(link).ConfigureAwait(false);
            request.Text = websiteText ?? throw new BadRequestException("Failed to get text from link");
            return await Compare(request).ConfigureAwait(false);
        }

        public async Task<RequestResult> Compare(TextCheckRequest request)
        {
            var originalText = request.Text;
            bool enableInternetSearch = true;
            var algorithm = Enum.IsDefined(typeof(SimilarityAlgorithm), request.Algotithm) ?
                (SimilarityAlgorithm)request.Algotithm : SimilarityAlgorithm.SorensenDice;

            _logger.LogInformation($"{Environment.NewLine} ///////// Start request");
            if (originalText.Length < 100)
            {
                throw new BadRequestException("Minimal length of text is 100 symbols");
            }

            originalText = _textHandler.StripTags(originalText);

            RequestResult result = new RequestResult();

            string text = _textHandler.GetTextWithoutSubstitutions(originalText, out int numberOfReplacements);
            result.Replacements = numberOfReplacements;
            var words = text.GetWords();
            result.Words = words.Length;

            List<string> internetLinks = null;
            var sw = new Stopwatch();
            sw.Start();
            Task<IEnumerable<string>> taskLinks = null;

            _statusManager.AddOrUpdate(request.Key, RequestStatus.InternetSearch);
            if(enableInternetSearch)
                taskLinks = _internetSearchApi.FindSitesAsync(words, request.MaxSites);

            var textProfile = _shinglesService.GetProfile(text, request.ShingleLength);
            var shinglesToWordIndexes = _shinglesService.GetShingleToWordIndexes(text, request.ShingleLength);

            if (enableInternetSearch)
            {
                var links = await taskLinks.ConfigureAwait(false);

                if (links.Count() == 0)
                {
                    result.Warning = "The number of requests to the Internet is exhausted";
                }
                else if (!string.IsNullOrEmpty(request.Link))
                {
                    // remove source link
                    links = links.Where(x => !x.Equals(request.Link)).ToArray();
                }

                internetLinks = new List<string>(links);
            }
            else
            {
                internetLinks = new List<string>();
            }

            sw.Stop();
            _logger.LogInformation($"Search in Internet finished, found links: {internetLinks.Count}. Time: {sw.ElapsedMilliseconds} ms");
            _statusManager.AddOrUpdate(request.Key, RequestStatus.Comparing);

            var compareAlgorithm = _algorithmsFactory.GetAlgorithm(algorithm);
            int[][] textToHighlights = new int[internetLinks.Count()][];
            var matchedTexts = new List<MatchedText>();

            sw = new Stopwatch();
            sw.Start();

            var tasks = new List<Task<(MatchedText match, int[] highlights)>>();
            for (var fileId=0; fileId< internetLinks.Count(); fileId ++)
            {
                tasks.Add(CompareText(fileId, 
                    internetLinks[fileId],
                    compareAlgorithm, 
                    textProfile,
                    words,
                    shinglesToWordIndexes,
                    request));
            }

            var allTask = Task.WhenAll(tasks);

            try
            {
                await allTask.ConfigureAwait(false);
            }
            catch (Exception)
            {
                allTask.Exception.InnerExceptions.Apply(x => _logger.LogError(999, x, "Text compare error"));
            }

            var compares = tasks.Where(x => x.Exception == null && x.Result.match != null);

            foreach (var compare in compares)
            {
                var temp = compare.Result;
                matchedTexts.Add(temp.match);
                textToHighlights[temp.match.Id] = temp.highlights;
            }

            sw.Stop();
            _logger.LogInformation($"Compare time: {sw.ElapsedMilliseconds}  ///////// {Environment.NewLine}");
            _statusManager.AddOrUpdate(request.Key, RequestStatus.FinalCalculations);

            var mathedTextesOrdered = matchedTexts.OrderByDescending(x => x.Percent).ToArray();
            string formatOfText = text.GetFormatOfText();
            var wordSpans = GetSpansForEachWord(textToHighlights, words);
            result.Text = string.Format(formatOfText, wordSpans);
            result.Matches = mathedTextesOrdered;
            var matchedWordsCount = GetCountOfAllMatchedWords(textToHighlights, words.Length);
            result.Percent = Math.Round(100 - (matchedWordsCount / (double)words.Length) * 100, 2);

            return result;
        }

        #region Private Methods 

        private async Task<(MatchedText, int[])> CompareText(int fileId, string file, ISimilarityDetector compareAlgorithm,
            IDictionary<string, int> textProfile, string[] words, IDictionary<string, List<int>> shinglesToWordIndexes, TextCheckRequest request)
        {
            var compareWatch = new Stopwatch();
            compareWatch.Start();

            #region Compare of each text

            string otherText = await GetTextFromUrl(file).ConfigureAwait(false);

            if (string.IsNullOrEmpty(otherText))
            {
                return (null, null);
            }

            var otherProfile = _shinglesService.GetProfile(otherText, request.ShingleLength);
            double similarity = compareAlgorithm.Similarity(textProfile, otherProfile);

            if (similarity == 0)
                return (null, null);

            var otherWords = otherText.GetWords();
            var biggerCountOfWords = words.Length > otherText.Length ? words.Length : otherWords.Length;
            var smallerCountOfWords = words.Length > otherText.Length ? otherWords.Length : words.Length;
            double lengthCoefficient = smallerCountOfWords / (double)biggerCountOfWords;
            double finalCoefficientOfAlike = request.Alike * lengthCoefficient;
            compareWatch.Stop();
            _logger.LogInformation($"Similarity {file}  = {similarity}, Allowed similarity: {finalCoefficientOfAlike}, Time for url: {compareWatch.ElapsedMilliseconds} ms");
            if (similarity < finalCoefficientOfAlike)
            {
                return (null, null);
            }

            #endregion

            #region Highlight words

            var equalShingles = textProfile.GetEqualShingles(otherProfile);
            int[] highlightsOfCurrentText = new int[words.Length];

            HashSet<int> wordIndexesToHighlight = new HashSet<int>();

            foreach (var equalShingle in equalShingles)
            {
                wordIndexesToHighlight.UnionWith(shinglesToWordIndexes[equalShingle]);
            }

            foreach (var wordIndex in wordIndexesToHighlight)
            {
                highlightsOfCurrentText[wordIndex] = 1;
            }

            #endregion

            MatchedText matchedText = new MatchedText();
            matchedText.Id = fileId;
            matchedText.Url = file;
            matchedText.Percent = Math.Round((wordIndexesToHighlight.Count / (double)words.Length) * 100, 2);
            return (matchedText, highlightsOfCurrentText);
        }

        private async Task<string> GetTextFromUrl(string path)
        {
            if (path.StartsWith("http", System.StringComparison.OrdinalIgnoreCase))
            {
                try
                {
                    var otherText = await _htmlParser.GetTextOfSiteAsync(path).ConfigureAwait(false);

                    if (!string.IsNullOrEmpty(otherText))
                    {
                        otherText = _textHandler.StripTags(otherText);
                        return otherText;
                    }
                    else
                    {
                        _logger.LogWarning(LoggingEvents.FailedToGetText, $"failed to get text from {path}");
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogWarning(LoggingEvents.FailedToGetText, $"failed to get text from {path}", ex);
                }
            }

            return null;
        }

        private StringBuilder[] GetSpansForEachWord(int[][] textToHighlights, string[] words)
        {
            // set class for each word (what word highlight in each text)
            var wordsSpan = new StringBuilder[words.Length];
            for (int textIndex = 0; textIndex < textToHighlights.Length; textIndex++)
            {
                if (textToHighlights[textIndex] == null)
                    continue;

                for (int wordIndex = 0; wordIndex < textToHighlights[textIndex].Length; wordIndex++)
                {
                    if (textToHighlights[textIndex][wordIndex] == 1)
                    {
                        if (wordsSpan[wordIndex] == null)
                        {
                            wordsSpan[wordIndex] = new StringBuilder("<span class='");
                        }

                        wordsSpan[wordIndex].Append($"t{textIndex} ");
                    }
                }
            }

            for (int wordSpanIndex = 0; wordSpanIndex < wordsSpan.Length; wordSpanIndex++)
            {
                if (wordsSpan[wordSpanIndex] == null)
                {
                    // set only word if it is unique for all texts 
                    wordsSpan[wordSpanIndex] = new StringBuilder(words[wordSpanIndex]);
                    continue;
                }
                else
                {
                    wordsSpan[wordSpanIndex].Append($"'>{words[wordSpanIndex]}</span>");
                }
            }

            return wordsSpan;
        }

        private int GetCountOfAllMatchedWords(int[][] textToHighlights, int wordsCount)
        {
            int allTextsMatchedWordsCount = 0;
            var tempVector = new int[wordsCount];
            foreach (var textVector in textToHighlights)
            {
                if (textVector == null)
                    continue;

                for (int i = 0; i < textVector.Length; i++)
                {
                    if (tempVector[i] == 1 || textVector[i] == 0)
                        continue;

                    tempVector[i] = 1;
                    allTextsMatchedWordsCount++;
                }
            }

            return allTextsMatchedWordsCount;
        }

        #endregion
    }
}
