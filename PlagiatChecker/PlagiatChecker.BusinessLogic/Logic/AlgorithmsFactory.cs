﻿using PlagiatChecker.BusinessLogic.Algorithms;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlagiatChecker.BusinessLogic.Logic
{
    public enum SimilarityAlgorithm
    {
        Cosine, 
        Jaccard,
        JaroWinkler,
        SorensenDice
    }

    public class AlgorithmsFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public AlgorithmsFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public ISimilarityDetector GetAlgorithm(SimilarityAlgorithm algorithm)
        {
            switch (algorithm)
            {
                case SimilarityAlgorithm.Cosine:
                    return (ISimilarityDetector)_serviceProvider.GetService(typeof(Cosine));
                case SimilarityAlgorithm.Jaccard:
                    return (ISimilarityDetector)_serviceProvider.GetService(typeof(Jaccard));
                case SimilarityAlgorithm.JaroWinkler:
                    return (ISimilarityDetector)_serviceProvider.GetService(typeof(JaroWinkler));
                case SimilarityAlgorithm.SorensenDice:
                    return (ISimilarityDetector)_serviceProvider.GetService(typeof(SorensenDice));
                default:
                    return (ISimilarityDetector)_serviceProvider.GetService(typeof(SorensenDice));
            }
        }
    }
}
