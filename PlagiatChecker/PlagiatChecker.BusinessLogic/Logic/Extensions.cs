﻿using PlagiatChecker.BusinessLogic.Stemmers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace PlagiatChecker.BusinessLogic.Logic
{
    public static class Extensions
    {
        private const string WORD_PATTERN = @"[^\W\d](\w|[-']{1,2}(?=\w))*";

        public static string[] GetWords(this string text, bool toSmall = false)
        {
            //var matches = Regex.Matches(text, @"\b[\w']*\b");
            //var words = from m in matches.Cast<Match>()
            //            where !string.IsNullOrEmpty(m.Value)
            //            select m.Value;

            MatchCollection matches = Regex.Matches(text, WORD_PATTERN);
            var words = matches.Cast<Match>().Select(x => toSmall ? x.Value.ToLower() : x.Value);
            return words.ToArray();
        }

        /// <summary>
        /// Returns string with formats except words of text, but keeps all formattings (new lines, spaces etc.)
        /// </summary>
        public static string GetFormatOfText(this string text)
        {
            //var formatString = Regex.Replace(text, @"\w+", match => {
            //    var res = $"{{{i}}}";
            //    i++;
            //    return res;
            //});

            int i = 0;
            var formatString = Regex.Replace(text, WORD_PATTERN, match => {
                var res = $"{{{i}}}";
                i++;
                return res;
            });

            return formatString;
        }

        /// <summary>
        /// Returns text splitted by whitespaces, but preservs tabs and new lines
        /// </summary>
        //public static string[] GetWordsWithFormatting(this string text)
        //{
        //    // "(\r\n)+" - finds all new lines, " $&" - adds to new lines one whitespace for correct divide
        //    // by whitespaces
        //    var temp = Regex.Replace(text, @"(\r\n)+", " $&");
        //    return Regex.Replace(temp, @"(\t)+", " $&")
        //        .Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
        //}

        public static IEnumerable<string> GetEqualShingles(this IDictionary<string, int> profile1, IDictionary<string, int> profile2)
        {
            var result = new List<string>();

            foreach (var key in profile1.Keys)
            {
                if (profile2.ContainsKey(key))
                    result.Add(key);
            }

            return result;
        }

        public static string[] GetWordsStemmers(this string[] words)
        {
            RussianStemmer ruStemmer = new RussianStemmer();
            EnglishStemmer engStemmer = new EnglishStemmer();
            List<string> result = new List<string>();
            string wordStemmer = null;

            foreach (string word in words)
            {
                if (string.IsNullOrEmpty(word))
                {
                    result.Add(word);
                    continue;
                }

                if (Regex.IsMatch(word, @"\p{IsCyrillic}"))
                {
                    // there is at least one cyrillic character in the string
                    wordStemmer = ruStemmer.Stem(word);
                }
                else
                {
                    wordStemmer = engStemmer.Stem(word);
                }

                result.Add(wordStemmer);
            }

            return result.ToArray();
        }

        public static IEnumerable<Y> ForEach<T, Y>(this IEnumerable<T> source, Func<T, Y> action)
        {
            if (source == null)
                throw new ArgumentException("source");
            if(action == null)
                throw new ArgumentException("action");

            List<Y> result = new List<Y>();
            foreach (T element in source)
            {
                result.Add(action(element));
            }

            return result;
        }

        public static void Apply<T>(this IEnumerable<T> source, Action<T> action)
        {
            if (source == null)
                throw new ArgumentException("source");
            if (action == null)
                throw new ArgumentException("action");

            foreach (T element in source)
            {
                action(element);
            }
        }
    }
}
