﻿using DataAccessLayer.UnitOfWork;
using PlagChecker.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace PlagiatChecker.BusinessLogic.Logic
{
    public interface ITextHandler
    {
        string GetText(string path);
        string[] SplitOnSentences(string text);
        IEnumerable<string> GetWordsWithSubstitutions(params string[] words);
        string GetTextWithoutSubstitutions(string text, out int count);
        string GetTextWithoutSubstitutions(string text);
        int[] GetSentencesIndexes(string[] textSentences);
        string StripTags(string source);
    }

    public class TextHandler : ITextHandler
    {
        private const string SEPARATOR_SYMBOL = "‡‡";
        IUnitOfWork _db;
        private Regex _htmlRegex = new Regex("<.*?>", RegexOptions.Compiled);

        public TextHandler(IUnitOfWork db)
        {
            _db = db;
        }

        #region Public Methods

        /// <summary>
        /// Remove HTML from string with compiled Regex.
        /// </summary>
        public string StripTags(string source)
        {
            return _htmlRegex.Replace(source, string.Empty);
        }

        /// <summary>
        /// Reads all the text from a file
        /// </summary>
        /// <param name="path">path of file</param>
        /// <returns>text</returns>
        public string GetText(string path)
        {
            FileInfo fileInfo = new FileInfo(path);
            string res = "";

            if (fileInfo.Extension == ".doc" || fileInfo.Extension == ".docx")
            {
                //using (WordDocument doc = new WordDocument())
                //{
                //    doc.Open(path);
                //    res = doc.ReadAllText(false);
                //}
            }
            else if (fileInfo.Extension == ".pdf")
            {
                //res = PDFParser.ExtractText(path);
            }
            else
            {
                res = File.ReadAllText(path, Encoding.Unicode);
            }

            return res;
        }

        /// <summary>
        /// Splites the text on sentences
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public string[] SplitOnSentences(string text)
        {
            // replace uncorrect finish of sentences
            foreach (Abbreviation item in _db.Abbreviations.Find(x => x.DotFirst == false))
            {
                text = Regex.Replace(text, string.Format(@"(?<=[\s+\(]{0})\.", item.Value), SEPARATOR_SYMBOL, RegexOptions.IgnoreCase);
            }

            // split on sentences
            string[] result = Regex.Split(text, @"(?<=[\]\)'""\w][\.\!\?])\s+(?=[А-ЯA-Z])");

            // restore uncorrect dots
            for (int i = 0; i < result.Length; i++)
            {
                string sentence = result[i];

                foreach (Abbreviation item in _db.Abbreviations.Find(x => x.DotFirst == false))
                {
                    sentence = Regex.Replace(sentence, string.Format(@"(?<=[\s+\(]{0}){1}", item.Value, SEPARATOR_SYMBOL), ".", RegexOptions.IgnoreCase);
                }

                result[i] = sentence;
            }

            return result;
        }

        /// <summary>
        /// Gets words with substituted symbols cyrilic - litin
        /// </summary>
        public IEnumerable<string> GetWordsWithSubstitutions(params string[] words)
        {
            List<string> result = new List<string>();

            foreach (var word in words)
            {
                if (Regex.IsMatch(word, @"\p{IsCyrillic}") && Regex.IsMatch(word, @"[A-Za-z]"))
                {
                    result.Add(word);
                }
            }

            return result;
        }

        public string GetTextWithoutSubstitutions(string text)
        {
            int temp = 0;
            return GetTextWithoutSubstitutions(text, out temp);
        }

        public string GetTextWithoutSubstitutions(string text, out int count)
        {
            count = 0;
            text = SubstitudeDashesAndQuotes(text);
            
            string[] words = text.GetWords();
            var substitutions = _db.Substitutions.SelectAll().ToList();

            foreach (var word in words)
            {
                if (Regex.IsMatch(word, @"\p{IsCyrillic}") && Regex.IsMatch(word, @"[A-Za-z]"))
                {
                    count++;
                    string newWord = word;

                    // change symbols to cyrillic
                    foreach (Substitution sub in substitutions)
                    {
                        newWord = newWord.Replace(sub.Latin, sub.Cyrillic);
                    }

                    // compare one more time
                    if (Regex.IsMatch(newWord, @"\p{IsCyrillic}") && Regex.IsMatch(newWord, @"[A-Za-z]"))
                    {
                        // change symbols to latin
                        foreach (Substitution sub in substitutions)
                        {
                            newWord = newWord.Replace(sub.Cyrillic, sub.Latin);
                        }
                    }

                    if (!(Regex.IsMatch(newWord, @"\p{IsCyrillic}") && Regex.IsMatch(newWord, @"[A-Za-z]")))
                    {
                        text = text.Replace(word, newWord);
                    }
                }
            }

            return text;
        }

        public int[] GetSentencesIndexes(string[] textSentences)
        {
            List<int> result = new List<int>();
            result.Add(0);

            foreach (var sentence in textSentences)
            {
                var words = sentence.Split(' ');
                result.Add(words.Count() + result.Last()); // index of first word of next sentence
            }

            return result.ToArray();
        }

        #endregion

        #region Private Methods

        private string SubstitudeDashesAndQuotes(string text)
        {
            text = text.Replace("﹘", "-");
            text = text.Replace("⸻", "-");
            text = text.Replace("⸺", "-");
            text = text.Replace("―", "-");
            text = text.Replace("—", "-");

            text = text.Replace("«", "'");
            text = text.Replace("»", "'");
            text = text.Replace("„", "'");
            text = text.Replace("“", "'");
            text = text.Replace("‘", "'");
            text = text.Replace("’", "'");
            text = text.Replace("''", "'");

            return text;
        }

        #endregion
    }
}
