﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlagiatChecker.BusinessLogic.Stemmers
{
    public interface IStemmer
    {
        string Stem(string s);
    }
}
