﻿namespace PlagiatChecker.Models
{
    public class TextCheckRequest
    {
        public string Text { get; set; }
        public string Link { get; set; }
        public int Algotithm { get; set; } = 3;
        public double Alike { get; set; } = 0.25;
        public int MaxSites { get; set; } = 15;
        public int ShingleLength { get; set; } = 3;
        public string Key { get; set; }
    }
}
