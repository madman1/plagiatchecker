﻿namespace PlagChecker.Models
{
    public class MatchedText
    {
        /// <summary>
        /// Text id
        /// </summary>
        public int Id { get; set; }
        /// <summary>
        /// Url of matched text
        /// </summary>
        public string Url { get; set; }
        /// <summary>
        /// Percent of alike with matched text
        /// </summary>
        public double Percent { get; set; }
    }
}
