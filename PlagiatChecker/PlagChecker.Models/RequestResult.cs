﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlagChecker.Models
{
    public class RequestResult
    {
        /// <summary>
        /// Error of request (null if all is ok)
        /// </summary>
        public string Error { get; set; }
        /// <summary>
        /// Warnings
        /// </summary>
        public string Warning { get; set; }
        /// <summary>
        /// Text after handling (without substitutions and extra symbols)
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Overall percent of text unique
        /// </summary>
        public double Percent { get; set; }
        /// <summary>
        /// Matched texts
        /// </summary>
        public MatchedText[] Matches { get; set; }
        /// <summary>
        /// Number of replacements
        /// </summary>
        public int Replacements { get; set; }
        /// <summary>
        /// Number of words
        /// </summary>
        public int Words { get; set; }
    }
}
