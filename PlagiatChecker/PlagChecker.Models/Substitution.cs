﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlagChecker.Models
{
    public class Substitution
    {
        public int Id { get; set; }
        public string Cyrillic { get; set; }
        public string Latin { get; set; }
    }
}
