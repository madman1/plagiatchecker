﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlagChecker.Models
{
    public class Abbreviation
    {
        public int Id { get; set; }
        public string Value { get; set; }
        public bool DotFirst { get; set; }
    }
}
