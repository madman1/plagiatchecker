﻿using DataAccessLayer.Repository;
using PlagChecker.Models;
using System;

namespace DataAccessLayer.UnitOfWork
{
    public class AntiplagiatUnitOfWork : IUnitOfWork
    {
        private DatabaseContext _db;

        public AntiplagiatUnitOfWork(DatabaseContext context,
            IRepository<Abbreviation> abbreviationsRepository,
            IRepository<Substitution> substitutionsRepository)
        {
            _db = context;
            Abbreviations = abbreviationsRepository;
            Substitutions = substitutionsRepository;
        }

        public IRepository<Abbreviation> Abbreviations { get; private set; }
        public IRepository<Substitution> Substitutions { get; private set; }

        public void Save()
        {
            _db.SaveChanges();
        }

        private bool disposed = false;
        public virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    _db.Dispose();
                }
                this.disposed = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
