﻿using DataAccessLayer.Repository;
using PlagChecker.Models;
using System;

namespace DataAccessLayer.UnitOfWork
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Abbreviation> Abbreviations { get; }
        IRepository<Substitution> Substitutions { get; }
        void Save();
    }
}
