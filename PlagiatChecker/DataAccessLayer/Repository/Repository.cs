﻿using DataAccessLayer.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DataAccessLayer
{
    public class Repository<T> : IRepository<T> where T : class
    {
        protected DatabaseContext DBContext { get; set; }

        public Repository(DatabaseContext dataContext)
        {
            DBContext = dataContext;
        }

        public void Insert(T entity)
        {
            DBContext.Set<T>().Add(entity);
        }

        public void Delete(T entity)
        {
            DBContext.Set<T>().Remove(entity);
        }

        public void Update(T entity, T newValue)
        {
            var entry = DBContext.Entry<T>(entity);
            entry.CurrentValues.SetValues(newValue);
            entry.State = EntityState.Modified;
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return DBContext.Set<T>().Where(predicate);
        }

        public IQueryable<T> SelectAll()
        {
            return DBContext.Set<T>();
        }

        public void SubmitAll()
        {
            DBContext.SaveChanges();
        }
    }
}
