﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace DataAccessLayer.Repository
{
    public interface IRepository<T>
    {
        void Insert(T entity);
        void Delete(T entity);
        void Update(T entity, T newValue);
        IQueryable<T> Find(Expression<Func<T, bool>> predicate);
        IQueryable<T> SelectAll();
        void SubmitAll();
    }
}
