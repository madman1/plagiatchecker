﻿using Microsoft.EntityFrameworkCore;
using PlagChecker.Models;

namespace DataAccessLayer
{
    public class DatabaseContext : DbContext
    {
        private readonly string _connectionString;

        public DatabaseContext(string connectionString)
        {
            _connectionString = connectionString;
        }
        public DbSet<Abbreviation> Abbreviations { get; set; }
        public DbSet<Substitution> Substitutions { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseMySql(_connectionString);
        }
    }
}
