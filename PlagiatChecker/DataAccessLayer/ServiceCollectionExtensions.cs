﻿using DataAccessLayer.Repository;
using DataAccessLayer.UnitOfWork;
using Microsoft.Extensions.DependencyInjection;
//using MySQL.Data.EntityFrameworkCore.Extensions;

namespace DataAccessLayer
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection InitialiseDataLayerDependencies(this IServiceCollection serviceCollection, string connection)
        {
            serviceCollection.AddScoped(provider => new DatabaseContext(connection));
            //serviceCollection.AddDbContext<DatabaseContext>(options => options.UseMySql(connection));

            serviceCollection.AddScoped<IUnitOfWork, AntiplagiatUnitOfWork>();
            serviceCollection.AddScoped(typeof(IRepository<>), typeof(Repository<>));

            return serviceCollection;
        }
    }
}
