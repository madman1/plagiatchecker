﻿using DataAccessLayer.UnitOfWork;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PlagChecker.Models;
using PlagiatChecker.BusinessLogic.HelpFiles;
using PlagiatChecker.BusinessLogic.Logic;
using PlagiatChecker.Filters;
using PlagiatChecker.Models;
using System.Threading.Tasks;

namespace PlagiatChecker.Controllers
{
    public class HomeController : Controller
    {
        private readonly TextComparator _textComparator;
        private readonly ILogger<HomeController> _logger;
        private readonly StatusManager _statusManager;

        public HomeController(TextComparator textComparator,
            ILogger<HomeController> logger,
            StatusManager statusManager)
        {
            _textComparator = textComparator;
            _logger = logger;
            _statusManager = statusManager;
        }

        public IActionResult Index()
        {
            Test t = new Test();
            return View(t);
        }

        [HttpPost]
        [Route("api/plag")]
        [ValidateModel]
        public async Task<IActionResult> FindPlag([FromBody] CompareRequest request)
        { 
            var req = new TextCheckRequest
            {
                Text = request.Text,
                Algotithm = request.Algotithm,
                Alike = request.Alike,
                MaxSites = request.MaxSites,
                ShingleLength = request.ShingleLength,
                Key = request.Key
            };

            RequestResult res = null;
            try
            {
                _statusManager.AddOrUpdate(req.Key, RequestStatus.Processing);
                res = await _textComparator.Compare(req);
            }
            finally
            {
                _statusManager.RemoveKey(req.Key);
            }

            return new ObjectResult(res);
        }

        [HttpPost]
        [Route("api/plag/link")]
        [ValidateModel]
        public async Task<IActionResult> FindPlagFromLink([FromBody] CheckWebsiteRequest request)
        {
            var req = new TextCheckRequest
            {
                Link = request.Link,
                Algotithm = request.Algotithm,
                Alike = request.Alike,
                MaxSites = request.MaxSites,
                ShingleLength = request.ShingleLength,
                Key = request.Key
            };

            RequestResult res = null;
            try
            {
                _statusManager.AddOrUpdate(req.Key, RequestStatus.Processing);
                res = await _textComparator.CompareWebsite(req);
            }
            finally
            {
                _statusManager.RemoveKey(req.Key);
            }

            return new ObjectResult(res);
        }

        [HttpGet]
        [Route("api/status")]
        public IActionResult GetStatus(string key)
        {
            if (string.IsNullOrEmpty(key))
            {
                return new BadRequestResult();
            }

            return new ObjectResult(_statusManager.GetValue(key));
        }
    }
}