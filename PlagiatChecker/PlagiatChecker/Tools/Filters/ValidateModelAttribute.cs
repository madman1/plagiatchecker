﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PlagiatChecker.Filters
{
    public class ValidateModelAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            if (!context.ModelState.IsValid)
            {
                var messages = context.ModelState.SelectMany(x => x.Value.Errors).Select(x => x.ErrorMessage).ToArray();
                var res = string.Join("\n", messages);
                context.Result = new BadRequestObjectResult(res);
            }
        }
    }
}
