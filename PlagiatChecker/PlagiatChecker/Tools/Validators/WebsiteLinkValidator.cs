﻿
using System;
using System.ComponentModel.DataAnnotations;

namespace PlagiatChecker.Tools.Validators
{
    public class WebsiteLinkValidator : ValidationAttribute
    {
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            string link = value.ToString();
            link = link.StartsWith("http") ? link : string.Concat("http://", link);

            bool goodLink = Uri.TryCreate(link, UriKind.Absolute, out Uri uriResult)
                && (uriResult.Scheme == Uri.UriSchemeHttp || uriResult.Scheme == Uri.UriSchemeHttps);

            return goodLink ? ValidationResult.Success : new ValidationResult(ErrorMessage);
        }
    }
}
