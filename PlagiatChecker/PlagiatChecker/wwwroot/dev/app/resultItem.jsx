import React from 'react';
import ReactDOM from 'react-dom';

export default class ResultItem extends React.Component {
    render() {
        return (
<li className="result-item">
    <a className="item-ref" href={this.props.match.url} target="_blank">{this.props.match.url}</a>
    <span className="item-percent">{this.props.match.percent}%</span>
    <button className="showBtn" onClick={this.highlightClick.bind(this)}>show</button>
</li>
        );
    }

    highlightClick(){
        this.props.highlightClick(this.props.match.id)
    }
}