import React from 'react';
import ReactDOM from 'react-dom';
import Guid from 'guid';

export default class CheckWebsiteModal extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      link: ""
    }
  }

  render() {
        return (
<div className="modal js-modal" onClick={this.closeModal.bind(this)}>
  <div className="modal-content">
      <span className="closeBtn js-closeModal" onClick={this.xClick.bind(this)}>&times;</span>
      <div className="modal-description">Please insert the link of the webpage</div>
      <form >
          <input className="control modal-control" onChange={this.linkChanged.bind(this)}  type="text" />
          <button type="button" className="plagBtn plagBtn--check" onClick={this.checkWebsite.bind(this)}>Check</button>
      </form>
  </div>
</div>
        );
  }

  linkChanged(e) {
    let value = e.target.value;
    this.setState({link: value});
  }

  xClick(){
    this.props.hideModal();
  }

  closeModal(event) {
    var modal = document.getElementsByClassName('js-modal')[0];

    if (event.target == modal) 
      this.props.hideModal();
  }

  checkWebsite(){
    this.props.checkWebsiteCallback(this.state.link);
    this.xClick();
  }
}