import React from 'react';
import ReactDOM from 'react-dom';
import Guid from 'guid';
import Loader from "./loader.jsx";
import CheckWebsiteModal from "./websiteModal.jsx";

export default  class TextChecker extends React.Component {
    
  componentWillReceiveProps()
  {
    return true;
  }
    constructor(props)
    {
      super(props);
      this.originalText = "";
      this.highlightSpans.bind(this);
      this.state = {text: "",
        isResult: false,
        hideBtns: false,
        showWebsiteModal: false
      };
    }

    componentDidUpdate() {
      if(this.state.isResult) {
        this.highlightSpans();
      }
    }

    componentDidMount() {
      if(this.state.isResult)
      {
        this.highlightSpans()
      }
    } 
    
    render() {
        return (
<div>
    <div className="text-wrapper">
      {this.state.isResult ?
        <div className="control textBox textBox--result"
          dangerouslySetInnerHTML={{__html: this.state.text}}></div>
        :
        <textarea placeholder="Enter Your Text Here"
          className="control textBox"
          value={this.state.text}
          onChange={this.textChanged.bind(this)}></textarea>
      }
    </div>
    { !this.state.hideBtns ?  
    <div className="buttons-wrapper">
      {this.state.isResult ?
      <button onClick={this.newCheckClick.bind(this)} className="plagBtn plagBtn--newSearch">New Search</button>
      :
      <div>
        <button onClick={this.checkClick.bind(this)} className="plagBtn plagBtn--check">Check</button>
        <button onClick={this.showWebsiteModal.bind(this)} className="plagBtn plagBtn--check">Website</button>
      </div>
      }
    </div>
    :
    <Loader requestKey={this.state.key}/>
    }

    {
      this.state.showWebsiteModal ? 
      <CheckWebsiteModal hideModal={this.hideWebsiteModal.bind(this)} checkWebsiteCallback={this.checkWebsiteClick.bind(this)}/> : <div></div>
    }
</div>);
    }

    textChanged(e) {
      let value = e.target.value;
      this.setState({text: value});
    }

    checkWebsiteClick(link) {
      let requestKey = Guid.raw();
      this.setState({hideBtns: true, key: requestKey});
      this.props.checkWebsiteCallback(link, requestKey);
    }

    checkClick() {
      let requestKey = Guid.raw();
      this.originalText = this.state.text;
      this.setState({hideBtns: true, key: requestKey});
      this.props.checkTextCallback(this.originalText, requestKey);
    }

    newCheckClick() {

      this.setState({text: this.originalText,
                isResult: false});
      this.props.hideResult();
    }

    highlightSpans(textId) {

      let spans = null;

      // clear all highlights first
      let allSpans = document.querySelectorAll('.textBox--result span');
      allSpans.forEach(function(element) {
        element.classList.remove('highlight');
      }, this);

      if(textId == null){
        spans = document.querySelectorAll('.textBox--result span');
      }
      else {
        spans = document.querySelectorAll('.textBox--result .t' + textId);
      }

      spans.forEach(function(element) {
        element.classList.add('highlight');
      }, this);
    }

    updateText(text){
        this.setState({text: text,
          isResult: true,
          hideBtns: false });
    }

    resetBtn(){
      this.setState({
        hideBtns: false });
    }

    showWebsiteModal() {
      this.setState({showWebsiteModal: true});
    }

    hideWebsiteModal() {
      this.setState({showWebsiteModal: false});
    }
}