import React from 'react';
import ReactDOM from 'react-dom';

export default class Header extends React.Component {

  constructor(props)
  {
    super(props);
    this.configClick = this.configClick.bind(this);
    this.state = {
      confActive: false
    };
  }

  render(){
    return (
    <header>
      <nav className="main-header">
        <a href="#" className="logo">Plagchecker</a>
        <ul className="main-navigation">
          <li className="navigation-item">
            <button onClick={this.configClick} className="configurationBtn">
              {"Сonfiguration " + (!this.state.confActive ? String.fromCharCode(9662) : String.fromCharCode(9652)) }
            </button>
          </li> 
        </ul>
      </nav>
    </header>
    );
  }

  configClick() {
    this.setState({confActive: !this.state.confActive});
    this.props.configClickCallback();
  }
}