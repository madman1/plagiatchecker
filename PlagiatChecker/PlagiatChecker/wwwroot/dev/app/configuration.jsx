import React from 'react';
import ReactDOM from 'react-dom';

export default class Configuration extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        coef: props.configuration.coef,
        algorithm: props.configuration.algorithm,
        maxSites: props.configuration.maxSites,
        shingle: props.configuration.shingle
      };
    }

    coefChanged(e){
      this.setState({coef: e.target.value});
    }

    algorithmChanged(e){
      this.setState({algorithm: e.target.value});
    }

    maxSitesChanged(e){
      this.setState({maxSites: e.target.value});
    }

    shingleChanged(e) {
      this.setState({shingle: e.target.value});
    }

    getConfiguration() {
      return {
        coef: this.state.coef,
        algorithm: this.state.algorithm,
        maxSites: this.state.maxSites,
        shingle: this.state.shingle
      }
    }

    render() {
        return (
<form className="config-bar">
  <div className="config-column">
    <div className="config-group">
        <label className="config-option" htmlFor="alike">Coefficient of alike</label>
        <input className="control control-number" id="alike" placeholder="from 0 to 1" type="text" onChange={this.coefChanged.bind(this)} value={this.state.coef}/>
    </div>
    <div className="config-group">
        <label className="config-option" htmlFor="algotithm">Algotithm</label>
        <select className="control" id="algotithm" value={this.state.algorithm} onChange={this.algorithmChanged.bind(this)}>
            <option value="0">Cosine</option>
            <option value="1">Jaccard</option>
            <option value="2">Jaro Winkler</option>
            <option value="3">Sorensen Dice</option>
        </select>
    </div>
  </div>
  <div className="config-column">
    <div className="config-group">
        <label className="config-option" htmlFor="maxSites">Max number of sites</label>
        <input className="control control-number" id="maxSites" type="text" value={this.state.maxSites} onChange={this.maxSitesChanged.bind(this)}/>
    </div>
    <div className="config-group">
        <label className="config-option" htmlFor="shingle">Shingle length</label>
        <input className="control control-number" id="shingle" placeholder="from 2 to 10" type="text" value={this.state.shingle} onChange={this.shingleChanged.bind(this)}/>
    </div>
  </div>
</form>
        );
    }
}