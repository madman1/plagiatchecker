import React from 'react';
import ReactDOM from 'react-dom';
import ResultItem from './resultItem.jsx';

export default class ResultBox extends React.Component {
    render() {
        return (
    <div className="result-wrapper">
        <div className="result-title">
            <div className="result-value">Originality: {this.props.result.percent}%</div>
            <button className="showBtn" onClick={this.highlightClick.bind(this)}>show all matches</button>
        </div>
        <ul>
            {
                this.props.result.matches
                .map(match => <ResultItem key={match.id} match={match} highlightClick={this.props.highlightClick}/>)
            }
            
        </ul>
    </div>

        );
    }

    highlightClick(){
        this.props.highlightClick()
    }
}