export default class AjaxHelper {

    constructor()
    {
        this.basicUrl = window.configuration.basePath;
    }
 

    ajaxGet(url, token)
    {
        let headers = new Headers();
        if(token != null)
        {
            headers.append("Authorization", "Bearer " + token);
        }

        let init = { method: 'GET',
                    headers: headers};
                    
        return fetch(this.basicUrl + url, init);
    }

    ajaxPost(url, body)
    {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");

        let res = JSON.stringify(body);

        let init = { method: 'POST',
                    headers: headers,
                    body: res};

        return fetch(this.basicUrl + url, init);
    }

    ajaxPostWithInfo(url, body)
    {
        let headers = new Headers();
        headers.append("Content-Type", "application/json");

        let jsonBody = JSON.stringify(body);

        let init = { method: 'POST',
                    headers: headers,
                    body: jsonBody};

        return fetch(this.basicUrl + url, init).then((response) => {
            return response;
        });
    }
}