import React from 'react';
import ReactDOM from 'react-dom';

import ResultBox from './resultbox.jsx';
import TextChecker from './textchecker.jsx';
import Configuration from './configuration.jsx';
import Header from './header.jsx';
import AjaxHelper from './services/ajaxHelper.js';

class App extends React.Component {

    constructor(props){
        super(props);
        this.getBody = this.getBody.bind(this);
        this.makeRequest = this.makeRequest.bind(this);
    }

    componentWillMount(){
        this.setState({
            resultObject: null,
            showConfig: false,
            configuration: {
                coef: 0.01,
                algorithm: 0,
                maxSites: 15,
                shingle: 2
            }
        });
    }

    render() {
        return (
<div>
    <Header configClickCallback={this.configClick.bind(this)}/>
    {this.state.showConfig ? 
    <Configuration configuration={this.state.configuration} ref={ins => {this.configuration = ins; }} /> 
    :
    <div></div>}
    <TextChecker ref={ins => { this.textChecker = ins; }} 
        hideResult={this.hideResult.bind(this)} 
        checkTextCallback={this.checkText.bind(this)}
        checkWebsiteCallback={this.checkWebsite.bind(this)}/>
    {this.state.resultObject == null ? 
        <div></div> :
        <ResultBox result={this.state.resultObject} highlightClick={this.highlightClick.bind(this)}/>}
</div>);
    }

    configClick() {
        let configuration = this.configuration ? this.configuration.getConfiguration() 
            : this.state.configuration;

        this.setState({showConfig: !this.state.showConfig,
            configuration: configuration});
    }

    resultIsReady(result) {
        this.setState({
            resultObject: result
        });
    }

    hideResult() { 
        this.setState({
            resultObject: null
        });
    }

    checkText(text, requestKey) {
      let body = this.getBody();
      body.text = text;
      body.key = requestKey;
      
      this.makeRequest("/api/plag", body);
    }

    checkWebsite(link, requestKey) {
        let body = this.getBody();
        body.link = link;
        body.key = requestKey;

        this.makeRequest("/api/plag/link", body);
    }

    makeRequest(url, body) {
        let helper = new AjaxHelper();

        helper.ajaxPost(url, body).then((response) => {
            this.textChecker.resetBtn();
            if(response.status >= 500) {
                alert("Something went wrong");
            }
            else if(response.status >= 400) {
                response.text().then(x=> {
                    if(x) {
                        alert(x);
                    }
                    else {
                        alert("Please set correct values");
                    }
                });
            }
            else if(response.status >= 200 && response.status < 300) {
                response.json().then(x=> {
                    this.resultIsReady(x);
                    this.textChecker.updateText(x.text)
                });
            }
        }).catch((reason) => {
            alert("Something went wrong"); });
    }

    getBody() {
        let conf = this.configuration ? this.configuration.getConfiguration() :
            this.state.configuration;

        return {
            alike: conf.coef,
            algotithm: conf.algorithm,
            maxSites: conf.maxSites,
            shingleLength: conf.shingle
        }
    }

    highlightClick(textId){
        this.textChecker.highlightSpans(textId);
    }
}

ReactDOM.render(<App/>, document.getElementById('container'));