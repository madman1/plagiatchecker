import React from 'react';
import ReactDOM from 'react-dom';
import AjaxHelper from './services/ajaxHelper.js';

export default class Loader extends React.Component {

  constructor(props) {
    super(props);
    this.checkStatus = this.checkStatus.bind(this);
    this.getStatusMsg = this.getStatusMsg.bind(this);
    this.ajaxHelper = new AjaxHelper();
    this.state = {
      message: ""
    }
  }

  componentWillMount(){
    this.interval  = setInterval(this.checkStatus, 1000);
  }

  componentWillUnmount(){
    clearInterval(this.interval);
  }

  render() {
        return (
<div className="loadContainer">
  <div className="loadCircle">
      <div className="circularG circularG_1"></div>
      <div className="circularG circularG_2"></div>
      <div className="circularG circularG_3"></div>
      <div className="circularG circularG_4"></div>
      <div className="circularG circularG_5"></div>
      <div className="circularG circularG_6"></div>
      <div className="circularG circularG_7"></div>
      <div className="circularG circularG_8"></div>
  </div>
  <span className="loadStatus">{this.state.message}</span>
</div>
        );
  }

  checkStatus() {
    this.ajaxHelper.ajaxGet('/api/status?key=' + this.props.requestKey).then(x=> {
      if(x.status >= 200 && x.status < 300)
      {
        x.text().then(status=> {
          let msg = this.getStatusMsg(status);
          this.setState({message: msg});
        });
      }
      else {
        this.setState({message: ""});
      }
      
    }).catch(x=> {this.setState({message: ""})});
  }

  getStatusMsg(status){
    switch(status) {
      case "0":
        return "Processing text";
      case "1": 
        return "Search similar texts";
      case "2": 
        return "Comparing found texts";
      case "3": 
        return "Final calculations";
      default:
        return "";
    }
  }
}