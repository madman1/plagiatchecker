var gulp = require('gulp');
var browserify = require('browserify');
var babelify = require('babelify');
var source = require('vinyl-source-stream');
var sass = require('gulp-sass');
var concat = require('gulp-concat');

// js build
var distFolder = '../dist';
var jsEntry = './app/index.jsx';
var jsxSource = 'app/**/*.jsx';

gulp.task('build', function () {
    return browserify({entries: jsEntry, extensions: ['.jsx', '.js'], debug: true})
        .transform('babelify', {presets: ['es2015', 'react']})
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(gulp.dest('../dist'));
});

gulp.task('watch', ['build'], function () {
    gulp.watch(jsxSource, ['build']);
});

// styles
var sassFiles = './scss/**/*.scss';

gulp.task('styles', function(){
    gulp.src(sassFiles)
        .pipe(concat("Styles.scss"))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulp.dest(distFolder));
});

gulp.task('watch styles',function() {
    gulp.watch(sassFiles,['styles']);
});

gulp.task('default', ['watch', 'watch styles']);