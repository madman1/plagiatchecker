﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Configuration;
using PlagiatChecker.BusinessLogic;
using DataAccessLayer;
using PlagiatChecker.Readers;

namespace PlagiatChecker
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public IHostingEnvironment HositngEnvironment { get; set; }

        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
            HositngEnvironment = env;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            var logicParams = Configuration.GetSection("YandexParams").Get<BusinessLogicParams>();
            logicParams.ProductionEnvironment = HositngEnvironment.IsProduction();

            services.AddMvc();
            services.InitialiseBusinessLogicDependencies(logicParams);
            services.InitialiseDataLayerDependencies(Configuration.GetSection("ConnectionString").Value);
            services.InitialiseReadersDependencies();
        }

        // Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole();
            loggerFactory.AddFile(Configuration.GetSection("FileLogging"));

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
