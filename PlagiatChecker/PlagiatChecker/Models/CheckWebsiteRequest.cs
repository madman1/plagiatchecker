﻿using PlagiatChecker.Tools.Validators;
using System;
using System.ComponentModel.DataAnnotations;

namespace PlagiatChecker.Models
{
    public class CheckWebsiteRequest : BaseCheckRequest
    {
        [Required]
        [StringLength(int.MaxValue, MinimumLength = 3, ErrorMessage = "Incorrect website link")]
        [WebsiteLinkValidator(ErrorMessage = "Incorrect website link")]
        public string Link { get; set; }
    }
}
