﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace PlagiatChecker.Models
{
    public class BaseCheckRequest
    {
        [Range(0, 4, ErrorMessage = "Incorrect value for Algotithm")]
        public int Algotithm { get; set; } = 3;
        [Range(0, 1, ErrorMessage = "Coefficient of alike can be in range from 0 to 1")]
        public double Alike { get; set; } = 0.25;
        [Range(1, int.MaxValue, ErrorMessage = "MaxSites can be only bigger than 0")]
        public int MaxSites { get; set; } = 15;
        [Range(2, 10, ErrorMessage = "Shingle length can be in range from 2 to 10")]
        public int ShingleLength { get; set; } = 3;

        [StringLength(40, MinimumLength = 10, ErrorMessage = "Request key is invalid")]
        public string Key { get; set; }
    }
}
