﻿using System.ComponentModel.DataAnnotations;

namespace PlagiatChecker.Models
{
    public class CompareRequest : BaseCheckRequest
    {
        [Required]
        [StringLength(int.MaxValue, MinimumLength = 100, ErrorMessage ="Minimal length of text is 100 symbols")]
        public string Text { get; set; }
    }
}
