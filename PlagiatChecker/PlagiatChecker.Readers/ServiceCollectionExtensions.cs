﻿using Microsoft.Extensions.DependencyInjection;

namespace PlagiatChecker.Readers
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection InitialiseReadersDependencies(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddScoped<PdfParser>();
            return serviceCollection;
        }
    }
}
