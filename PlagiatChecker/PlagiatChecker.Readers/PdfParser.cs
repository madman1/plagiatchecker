﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System.Text;

namespace PlagiatChecker.Readers
{
    public class PdfParser
    {
        /// <summary>
        /// Extracts a text from a PDF file.
        /// </summary>
        /// <param name="inFileName">the full path to the pdf file.</param>
        /// <returns>the extracted text</returns>
        public string ExtractText(string inFileName)
        {
            StringBuilder result = new StringBuilder();
            try
            {
                using (PdfReader reader = new PdfReader(inFileName))
                {
                    for (int page = 1; page <= reader.NumberOfPages; page++)
                    {
                        result.Append(PdfTextExtractor.GetTextFromPage(reader, page));
                    }
                }
            }
            catch
            {
                return result.ToString();
            }

            return result.ToString();
        }
    }
}
