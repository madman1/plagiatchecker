﻿using DataAccessLayer;
using DataAccessLayer.UnitOfWork;
using Microsoft.Extensions.DependencyInjection;
using PlagiatChecker.BusinessLogic;
using PlagiatChecker.BusinessLogic.Algorithms;
using PlagiatChecker.BusinessLogic.Algorithms.Fingerprinting;
using PlagiatChecker.BusinessLogic.Hashing;
using PlagiatChecker.BusinessLogic.HelpFiles;
using PlagiatChecker.BusinessLogic.Logic;
using PlagiatChecker.Readers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace TestProject
{
    class Program
    {
        static void Main(string[] args)
        {
            string html = File.ReadAllText("C:/Users/taras/Desktop/википедия/1.html");
            Console.WriteLine("Hello World!");

            var serviceProvider = new ServiceCollection()
                .InitialiseBusinessLogicDependencies(new BusinessLogicParams())
                .InitialiseDataLayerDependencies("server=localhost;UserId=root;Password=1111;database=antiplagiat;SslMode=none")
                .InitialiseReadersDependencies()
                .BuildServiceProvider();

            var pdfreader = serviceProvider.GetService<PdfParser>();

            var texg  = pdfreader.ExtractText(@"D:\projects\C#\Diploma\плагиаты\ньютон.pdf");

            var parser = serviceProvider.GetService<HtmlParser>();

            var ttt = parser.GetPlainTextFromHtml(html, null);

            IUnitOfWork antiplagiatUnitOf = serviceProvider.GetService<IUnitOfWork>();
            var tgdfgsd = antiplagiatUnitOf.Abbreviations;
            var ggsfgd = tgdfgsd.SelectAll().First();

            // var bar = serviceProvider.GetService<IBarService>();

            try
            {
                //GetSites("новости украина").Wait();

                ITextHandler t  = serviceProvider.GetService<ITextHandler>();

                var text = t.GetText(@"D:\projects\C#\Diploma\плагиаты\english_textf.txt");

                ///(\w)([\t])(\w)     $1***$3
                var test1 = text.GetWords().GetWordsStemmers();
                var test2 = text.GetWords();
                var test3 = System.Text.RegularExpressions.Regex.Replace(text, @"(\r\n)+", " $&").Split(new[] { " " }, StringSplitOptions.RemoveEmptyEntries);
                var temp4 = System.Text.RegularExpressions.Regex.Replace(text, @"(\r\n)+", " $&");
                var test4  = System.Text.RegularExpressions.Regex.Split(temp4, @" {1,}");

                int i = 0;
                var formatString = Regex.Replace(text, @"\w+", match => {
                    var res = $"{{{i}}}";
                    i++;
                    return res;
                });
                var fdslgjs = string.Format("{0}, {1}", "fdsf", "fsdgsdf");

                var original = t.GetText(@"D:\projects\C#\Diploma\плагиаты\2 мировая_ориг.txt");
                var copy = t.GetText(@"D:\projects\C#\Diploma\плагиаты\2 мировая _передел.txt");
                var bullText = t.GetText(@"D:\projects\C#\Diploma\плагиаты\2 моривая_сбулшитом.txt");
                var big = t.GetText(@"D:\projects\C#\Diploma\плагиаты\big_2мировая.txt");//big_2мировая
                var bigger = t.GetText(@"D:\projects\C#\Diploma\плагиаты\bigger_2мировая.txt");//big_2мировая
                var english = t.GetText(@"D:\projects\C#\Diploma\плагиаты\english_text.txt");//big_2мировая

                var someStr = "Вскоре после подписания Рапалльского договора";
                var someotherStr = "Вскоре после подписания Рапалльского договора секретное соглашение о сотрудничестве";
                var lol = "ВскореВскореВскореВскореВскореВскореВскореВскореВскореВскоре послепослепослепосле пbдписания Рапалльского договора";
                var lol1 = "ВскореВскореВскореВскореВскореВскореВскореВскореВскореВскоре подписания Рапалльского договора секретное соглашение о сотрудничестве";

                var originalText = english;
                var copyText = copy;
                ////
                var shingle = serviceProvider.GetService<ShinglesService>();
                var profile1 = shingle.GetProfile(originalText);
                var profile2 = shingle.GetProfile(copyText);

                /////

                var originalNumericHashes = shingle.GetNumericShingles(originalText);
                var copyNumericHashes = shingle.GetNumericShingles(copyText);

                SimHash simhash = new SimHash();
                var simhashSimilarity = simhash.GetSimilarity(originalNumericHashes, copyNumericHashes);

                MinHash mh = new MinHash(500);
                var hvs1 = mh.GetMinHashes(originalNumericHashes).ToList();
                var hvs2 = mh.GetMinHashes(copyNumericHashes).ToList();
                var minhres = Jaccard.Similarity(hvs1, hvs2);
                ////
                Jaccard jaccard = new Jaccard();
                var s = jaccard.Similarity(profile1, profile2);


                Cosine cosine = new Cosine();
                var c = cosine.Similarity(profile1, profile2);

                SorensenDice sorenDice = new SorensenDice();
                var sss = sorenDice.Similarity(profile1, profile2);

                // NOt shingles

                JaroWinkler winkler = new JaroWinkler();
                var kk = winkler.Similarity(profile1, profile2);

                
            }
            catch (Exception ex)
            {
                var ttttjskfljg = ex;
            }
        }

        public async static Task<IEnumerable<string>> GetSites(string searchQuery)
        {
            string key = "03.383751630:f5e7aefb05f30c38ce45c491982c3da6";
            string user = "antiplagiat-plavin";
            string address = "https://yandex.com/search/xml";
            string url = @"{0}?user={1}&key={2}&query={3}&l10n=en&sortby=rlv&filter=none&groupby=attr%3D%22%22.mode%3Dflat.groups-on-page%3D10.docs-in-group%3D1";

            string completeUrl = string.Format(url, address, user, key, searchQuery);
            Uri uri = new Uri(completeUrl);

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response = null;

            try
            {
                response = (HttpWebResponse)(await request.GetResponseAsync());
            }
            catch
            {
                throw;
            }

            XmlReader xmlReader = XmlReader.Create(response.GetResponseStream());
            XDocument xmlResponse = XDocument.Load(xmlReader);

            var groupQuery = xmlResponse.Elements().
               Elements("response").
               Elements("results").
               Elements("grouping").
               Elements("group").ToList();

            List<string> result = new List<string>();
            string urlQuery;

            for (int i = 0; i < groupQuery.Count(); i++)
            {
                urlQuery = groupQuery.ElementAt(i).Element("doc").Element("url").Value;
                result.Add(urlQuery);
            }

            return result;
        }
    }
}